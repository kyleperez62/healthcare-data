$CA_zip_codes = {
  "Alameda" => "94501", "Alpine" => "96120", "Amador" => "95629", "Calaveras" => "95222", "Colusa" => "95912", "Contra Costa" => "94505", 
  "Del Norte" => "95531", "El Dorado" => "95614", "Fresno" => "93210", "Glenn" =>"95920", "Imperial" => "92227", "Inyo" => "92328", 
  "Kern" => "93203", "Lassen" => "96056", "Los Angeles" => "90001", "Madera" => "93601", "Marin" => "94901", "Mariposa" => "93623", 
  "Mendocino" => "95410", "Merced" => "93620", "Modoc" => "96006", "Mono" => "93512", "Monterey" => "93426", "Napa" => "94503", 
  "Placer" => "95602", "Plumas" => "95923", "Riverside" => "91752", "Sacramento" => "95608", "San Benito" => "95023", "San Bernardino" => "91701", 
  "San Diego" => "91901", "San Francisco" => "94101", "San Joaquin" => "95202", "San Luis Obispo" => "93401", "San Mateo" => "94002", 
  "Santa Barbara" => "93013", "Santa Clara" => "94022", "Shasta" => "96001", "Siskiyou" => "95568", "Solano" => "94510", 
  "Sonoma" => "94922", "Stanislaus" => "95230", "Sutter" => "95645", "Tehama" => "96021", "Tulare" => "93207", "Tuolumne" => "95310", 
  "Ventura" => "91320", "Yolo" => "95605", "Yuba" => "95692"
}

$FL_zip_codes = {
  "Alachua" => "32601", "Baker" => "32040", "Bay" => "32401", "Brevard" => "32754", "Broward" => "33004", "Citrus" => "34428", " Collier" => "34102", 
  "Desoto" => "34266", "Dixie" => "32628", "Duval" => "32099", "Escambia" =>"32501", "Flagler" => "32110", "Gadsden" => "32324", 
  "Gilchrist" => "32619", "Glades" => "33471", "Gulf" => "32456", "Hardee" => "33834", "Hendry" => "33440", "Hernando" => "34601", 
  "Highlands" => "33825", "Holmes" => "32425", "Indian River" => "32948", "Lafayette" => "32066", "Lake" => "32102", "Leon" => "32301", 
  "Levy" => "32621", "Manatee" => "34201", "Miami-Dade" => "33010", "Okaloosa" => "32531", "Okeechobee" => "34972", "Osceola" => "34739", 
  "Palm Beach" => "33401", "Pasco" => "33523", "Pinellas" => "33701", "Santa Rosa" => "32561", "Sarasota" => "34223", 
  "Seminole" => "32701", "St Johns" => "32033", "St Lucie" => "34945", "Suwannee" => "32008", "Volusia" => "32114", 
  "Wakulla" => "32327"
}

$NC_zip_codes = {
  "Alamance" => "27215", "Alexander" => "28636", "Anson" => "28091", "Ashe" => "28615", "Avery" => "28604", "Beaufort" => "27806", 
  "Bertie" => "27805", "Bladen" => "28320", "Buncombe" => "28701", "Burke" =>"28612", "Cabarrus" => "28025", "Caldwell" => "28630", 
  "Carteret" => "28511", "Caswell" => "27212", "Catawba" => "28601", "Chatham" => "27207", "Cherokee" => "28901", "Chowan" => "27932", 
  "Cleveland" => "28020", "Columbus" => "28423", "Craven" => "28523", "Currituck" => "27916", "Dare" => "27948", "Davidson" => "27239", 
  "Davie" => "27006", "Duplin" => "28341", "Durham" => "27503", "Edgecombe" => "27801", "Forsyth" => "27009", "Gaston" => "28006", 
  "Gates" => "27926", "Graham" => "28702", "Granville" => "27507", "Guilford" => "27214", "Harnett" => "27501", 
  "Haywood" => "28716", "Henderson" => "28726", "Hertford" => "27818", "Hoke" => "28376", "Hyde" => "27824", 
  "Iredell" => "28115", "Johnston" => "27504", "Jones" => "28573", "Lenoir" => "28501", "Macon" => "28734", "Martin" => "27840", 
  "Mitchell" => "28705", "Moore" => "27242", "Nash" => "27557" , "New Hanover" => "28401", "Onslow" => "28445", "Pamlico" => "28510", 
  "Pasquotank" => "27909", "Pender" => "28421", "Perquimans" => "27919", "Person" => "27343", "Pitt" => "27812", "Polk" => "28722", "Robeson" => "28340", 
  "Rowan" => "27013", "Rutherford" => "28018", "Sampson" => "28318", "Scotland" => "28343", "Stanly" => "28001", "Stokes" => "27016", "Swain" => "28713", 
  "Transylvania" => "28708", "Tyrrell" => "27925", "Vance" => "27536", "Wake" => "27502", "Watauga" => "28605", "Wilkes" => "28606", "Wilson" => "27822", 
  "Yadkin" => "27011", "Yancey" => "28714"
}

$NY_zip_codes = {
  "Albany" => "12007", "Allegany" => "14708", "Bronx" => "10451", "Broome" => "13744", "Cattaraugus" => "14041", "Cayuga" => "13021", 
  "Chautauqua" => "14048", "Chemung" => "14814", "Chenango" => "13124", "Clinton" =>"12901", "Columbia" => "12017", "Cortland" => "13040", 
  "Delaware" => "12167", "Dutchess" => "12501", "Erie" => "14001", "Fulton" => "12025", "Genesee" => "14005", "Greene" => "12015", 
  "Hamilton" => "12108", "Herkimer" => "13324", "Jefferson" => "13601", "Kings" => "11201", "Lewis" => "13325", "Livingston" => "14414", 
  "Madison" => "13030", "Monroe" => "14420", "Montgomery" => "12010", "Nassau" => "11001", "New York" => "10001", "Niagara" => "14008", 
  "Oneida" => "13042", "Onondaga" => "13027", "Ontario" => "14424", "Oswego" => "13028", "Otsego" => "12064", 
  "Putnam" => "10509", "Queens" => "11004", "Rensselaer" => "12018", "Richmond" => "10301", "Rockland" => "10901", 
  "Saratoga" => "12019", "Schenectady" => "12008", "Schoharie" => "12031", "Schuyler" => "14805", "Seneca" => "13148", "St Lawrence" => "12922", 
  "Steuben" => "14572", "Tioga" => "13732", "Tompkins" => "13053", "Ulster" => "12401", "Wayne" => "13143", 
  "Westchester" => "10501", "Wyoming" => "14009", "Yates" => "14415"
}

$TX_zip_codes = {
  "Andrews" => "79714", "Angelina" => "75901", "Aransas" => "78382", "Archer" => "76366", "Atascosa" => "78008", "Austin" => "77418", 
  "Bailey" => "79320", "Bandera" => "78003", "Bastrop" => "78602", "Baylor" =>"76380", "Bee" => "78102", "Bexar" => "78002", 
  "Blanco" => "78606", "Borden" => "79738", "Bosque" => "76634", "Bowie" => "75501", "Brazoria" => "77422", "Brazos" => "77801", 
  "Brewster" => "79830", "Briscoe" => "79255", "Burleson" => "77836", "Burnet" => "78605", "Callahan" => "76443", "Camp" => "75451", 
  "Carson" => "79039", "Castro" => "79027", "Childress" => "79201", "Cochran" => "79346", "Coke" => "76933", "Coleman" => "76828", 
  "Collin" => "75002", "Collingsworth" => "79095", "Colorado" => "77434", "Comal" => "78070", "Concho" => "76837", 
  "Cooke" => "76238", "Coryell" => "76522", "Cottle" => "79223", "Crane" => "79731", "Crosby" => "79322", 
  "Culberson" => "79847", "Dallam" => "79022", "Deaf Smith" => "79045", "Denton" => "75007", "Dickens" => "79220", "Dimmit" => "78827", 
  "Donley" => "79226", "Eastland" => "76435", "Ector" => "79741", "El Paso" => "79821", "Erath" => "76401", "Falls" => "76524", 
  "Fisher" => "79534", "Foard" => "79227", "Fort Bend" => "77406", "Freestone" => "75838", "Frio" => "78005", "Gaines" => "79342", "Galveston" => "77510", 
  "Garza" => "79330", "Gillespie" => "78618", "Glasscock" => "79739", "Goliad" => "77963", "Gonzales" => "78122", "Gregg" => "75601", "Grimes" => "77363", 
  "Guadalupe" => "78108", "Hansford" => "79040", "Hartley" => "79018", "Hays" => "78610", "Hemphill" => "79014", "Hidalgo" => "78501", "Hockley" => "79313", 
  "Hood" => "76035", "Hudspeth" => "79839", "Hunt" => "75135", "Irion" => "76930", "Jack" => "76427", "Jim Hogg" => "78360", "Jim Wells" => "78332", 
  "Karnes" => "78111", "Kaufman" => "75114", "Kerr" => "78010", "Kimble" => "76849", "Kinney" => "78832", "Kleberg" => "78363", "Lamb" => "79031", 
  "Lampasas" => "76539", "Lavaca" => "77964", "Lipscomb" => "79005", "Live Oak" => "78022", "Llano" => "76831", "Lubbock" => "79329", "Lynn" => "79351", 
  "Matagorda" => "77414", "Maverick" => "78852", "McCulloch" => "76825", "McLennan" => "76557", "McMullen" => "78007", "Milam" => "76518", "Montague" => "76230", 
  "Motley" => "79234", "Nacogdoches" => "75760", "Navarro" => "75102", "Nolan" => "79506", "Nueces" => "78330", "Ochiltree" => "79070", "Palo Pinto" => "76067", 
  "Parker" => "76008", "Parmer" => "79009", "Pecos" => "79735", "Presidio" => "79843", "Rains" => "75440", "Randall" => "79015", "Reagan" => "76932", 
  "Real" => "78833", "Red River" => "75412", "Reeves" => "79718", "Refugio" => "77990", "Rockwall" => "75032", "Runnels" => "76821", "Sabine" => "75930", 
  "San Augustine" => "75929", "San Jacinto" => "77328", "San Patricio" => "78336", "San Saba" => "76832", "Schleicher" => "76936", "Scurry" => "79517", "Shackelford" => "76430", 
  "Somervell" => "76043", "Starr" => "78536", "Sterling" => "76951", "Stonewall" => "79502", "Sutton" => "76950", "Swisher" => "79052", "Tarrant" => "75054", 
  "Terry" => "79316", "Throckmorton" => "76483", "Titus" => "75455", "Tom Green" => "76901", "Travis" => "78617", "Trinity" => "75834", "Upton" => "79752", 
  "Uvalde" => "78801", "Val Verde" => "78837", "Van Zandt" => "75103", "Victoria" => "77901", "Waller" => "77423", "Webb" => "78040", "Wharton" => "77420", 
  "Wilbarger" => "76364", "Willacy" => "78569", "Winkler" => "79745", "Yoakum" => "79323", "Young" => "76372", "Zapata" => "78067", "Zavala" => "78829" 
  
}

$USA_zip_codes = {
  "CA" => $CA_zip_codes, "FL" => $FL_zip_codes, "NC" => $NC_zip_codes, 
  "NY" => $NY_zip_codes, "TX" => $TX_zip_codes
}


$test_response_data_quote = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns2:PlanQuoteResponse xmlns:ns2="http://hios.cms.org/api" xmlns="http://hios.cms.org/api-types">
    <ns2:ResponseHeader>
        <ReturnCode>Success</ReturnCode>
    </ns2:ResponseHeader>
    <ns2:TotalEligiblePlansQuantity>5</ns2:TotalEligiblePlansQuantity>
    <ns2:PaginationInformation>
        <PageNumber>1</PageNumber>
        <PageSize>20</PageSize>
    </ns2:PaginationInformation>
    <ns2:Plans>
        <ns2:Plan>
            <PlanID>96835VA0200001</PlanID>
            <PlanNameText>Anthem HealthKeepers Preferred DirectAccess w/Child Dental - cdda</PlanNameText>
            <ProductID>96835VA020</ProductID>
            <ProductNameText>Anthem HealthKeepers IFP 7 EPO</ProductNameText>
            <IssuerID>96835</IssuerID>
            <IssuerNameText>Assurant Health</IssuerNameText>
            <IssuerStateCode>VA</IssuerStateCode>
            <IssuerTollFreeNumber>18008001212</IssuerTollFreeNumber>
            <IssuerLocalNumber>14142713011</IssuerLocalNumber>
            <IssuerTTYNumber xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <ProviderType>EPO</ProviderType>
            <BaseRateAmount>226.00</BaseRateAmount>
            <MetalTierType>Gold</MetalTierType>
            <HSAEligibleIndicator>false</HSAEligibleIndicator>
            <ChildOnlyIndicator>true</ChildOnlyIndicator>
            <SameSexPartnerIndicator>false</SameSexPartnerIndicator>
            <DomesticPartnerIndicator>false</DomesticPartnerIndicator>
            <MaternityCoverageIndicator>true</MaternityCoverageIndicator>
            <MentalCoverageIndicator>true</MentalCoverageIndicator>
            <SubstanceAbuseCoverageIndicator>true</SubstanceAbuseCoverageIndicator>
            <DrugCoverageIndicator>true</DrugCoverageIndicator>
            <DentalCoverageIndicator>true</DentalCoverageIndicator>
            <VisionCoverageIndicator>true</VisionCoverageIndicator>
            <WellnessPorgramCoverageIndicator>false</WellnessPorgramCoverageIndicator>
            <FamilyAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </FamilyAnnualDeductibleAmount>
            <IndividualAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualDeductibleAmount>
            <FamilyAnnualOOPLimitAmount>
                <Amount>26.00</Amount>
            </FamilyAnnualOOPLimitAmount>
            <IndividualAnnualOOPLimitAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualOOPLimitAmount>
        </ns2:Plan>
        <ns2:Plan>
            <PlanID>96835VA0230001</PlanID>
            <PlanNameText>Anthem HealthKeepers Preferred DirectAccess - ccam</PlanNameText>
            <ProductID>96835VA023</ProductID>
            <ProductNameText>Anthem DirectAccess</ProductNameText>
            <IssuerID>96835</IssuerID>
            <IssuerNameText>Assurant Health</IssuerNameText>
            <IssuerStateCode>VA</IssuerStateCode>
            <IssuerTollFreeNumber>18008001212</IssuerTollFreeNumber>
            <IssuerLocalNumber>14142713011</IssuerLocalNumber>
            <IssuerTTYNumber xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <ProviderType>POS</ProviderType>
            <BaseRateAmount>335.00</BaseRateAmount>
            <MetalTierType>Bronze</MetalTierType>
            <HSAEligibleIndicator>false</HSAEligibleIndicator>
            <ChildOnlyIndicator>true</ChildOnlyIndicator>
            <SameSexPartnerIndicator>false</SameSexPartnerIndicator>
            <DomesticPartnerIndicator>false</DomesticPartnerIndicator>
            <MaternityCoverageIndicator>true</MaternityCoverageIndicator>
            <MentalCoverageIndicator>true</MentalCoverageIndicator>
            <SubstanceAbuseCoverageIndicator>true</SubstanceAbuseCoverageIndicator>
            <DrugCoverageIndicator>true</DrugCoverageIndicator>
            <DentalCoverageIndicator>true</DentalCoverageIndicator>
            <VisionCoverageIndicator>true</VisionCoverageIndicator>
            <WellnessPorgramCoverageIndicator>false</WellnessPorgramCoverageIndicator>
            <FamilyAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </FamilyAnnualDeductibleAmount>
            <IndividualAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualDeductibleAmount>
            <FamilyAnnualOOPLimitAmount>
                <Amount>20.00</Amount>
            </FamilyAnnualOOPLimitAmount>
            <IndividualAnnualOOPLimitAmount>
                <Amount>10.00</Amount>
            </IndividualAnnualOOPLimitAmount>
        </ns2:Plan>
        <ns2:Plan>
            <PlanID>50102VA0310005</PlanID>
            <PlanNameText>CoreMed - Silver 1 $3500, 100%</PlanNameText>
            <ProductID>50102VA031</ProductID>
            <ProductNameText>2014 EHB IM PLAN</ProductNameText>
            <IssuerID>50102</IssuerID>
            <IssuerNameText>Assurant Health</IssuerNameText>
            <IssuerStateCode>VA</IssuerStateCode>
            <IssuerTollFreeNumber>18008001212</IssuerTollFreeNumber>
            <IssuerLocalNumber>14142713011</IssuerLocalNumber>
            <IssuerTTYNumber xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <ProviderType>EPO</ProviderType>
            <BaseRateAmount>346.00</BaseRateAmount>
            <MetalTierType>Bronze</MetalTierType>
            <HSAEligibleIndicator>false</HSAEligibleIndicator>
            <ChildOnlyIndicator>true</ChildOnlyIndicator>
            <SameSexPartnerIndicator>false</SameSexPartnerIndicator>
            <DomesticPartnerIndicator>false</DomesticPartnerIndicator>
            <MaternityCoverageIndicator>true</MaternityCoverageIndicator>
            <MentalCoverageIndicator>true</MentalCoverageIndicator>
            <SubstanceAbuseCoverageIndicator>true</SubstanceAbuseCoverageIndicator>
            <DrugCoverageIndicator>true</DrugCoverageIndicator>
            <DentalCoverageIndicator>true</DentalCoverageIndicator>
            <VisionCoverageIndicator>true</VisionCoverageIndicator>
            <WellnessPorgramCoverageIndicator>false</WellnessPorgramCoverageIndicator>
            <FamilyAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </FamilyAnnualDeductibleAmount>
            <IndividualAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualDeductibleAmount>
            <FamilyAnnualOOPLimitAmount>
                <Amount>26.00</Amount>
            </FamilyAnnualOOPLimitAmount>
            <IndividualAnnualOOPLimitAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualOOPLimitAmount>
        </ns2:Plan>
        <ns2:Plan>
            <PlanID>96835VA0140007</PlanID>
            <PlanNameText>CoreMed - Gold 1 $2000, 100%</PlanNameText>
            <ProductID>96835VA014</ProductID>
            <ProductNameText>2014 EHB IM PLAN</ProductNameText>
            <IssuerID>96835</IssuerID>
            <IssuerNameText>Assurant Health</IssuerNameText>
            <IssuerStateCode>VA</IssuerStateCode>
            <IssuerTollFreeNumber>18008001212</IssuerTollFreeNumber>
            <IssuerLocalNumber>14142713011</IssuerLocalNumber>
            <IssuerTTYNumber xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <ProviderType>HMO</ProviderType>
            <BaseRateAmount>397.00</BaseRateAmount>
            <MetalTierType>Gold</MetalTierType>
            <HSAEligibleIndicator>false</HSAEligibleIndicator>
            <ChildOnlyIndicator>true</ChildOnlyIndicator>
            <SameSexPartnerIndicator>true</SameSexPartnerIndicator>
            <DomesticPartnerIndicator>true</DomesticPartnerIndicator>
            <MaternityCoverageIndicator>true</MaternityCoverageIndicator>
            <MentalCoverageIndicator>true</MentalCoverageIndicator>
            <SubstanceAbuseCoverageIndicator>true</SubstanceAbuseCoverageIndicator>
            <DrugCoverageIndicator>true</DrugCoverageIndicator>
            <DentalCoverageIndicator>true</DentalCoverageIndicator>
            <VisionCoverageIndicator>true</VisionCoverageIndicator>
            <WellnessPorgramCoverageIndicator>false</WellnessPorgramCoverageIndicator>
            <FamilyAnnualDeductibleAmount>
                <Amount>42.00</Amount>
            </FamilyAnnualDeductibleAmount>
            <IndividualAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualDeductibleAmount>
            <FamilyAnnualOOPLimitAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </FamilyAnnualOOPLimitAmount>
            <IndividualAnnualOOPLimitAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualOOPLimitAmount>
        </ns2:Plan>
        <ns2:Plan>
            <PlanID>50102VA0250005</PlanID>
            <PlanNameText>CoreMed - Platinum 2 $0, 75% (Copay)</PlanNameText>
            <ProductID>50102VA025</ProductID>
            <ProductNameText>2014 EHB IM PLAN</ProductNameText>
            <IssuerID>50102</IssuerID>
            <IssuerNameText>Assurant Health</IssuerNameText>
            <IssuerStateCode>VA</IssuerStateCode>
            <IssuerTollFreeNumber>18008001212</IssuerTollFreeNumber>
            <IssuerLocalNumber>14142713011</IssuerLocalNumber>
            <IssuerTTYNumber xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <ProviderType>HMO</ProviderType>
            <BaseRateAmount>422.00</BaseRateAmount>
            <MetalTierType>Gold</MetalTierType>
            <HSAEligibleIndicator>false</HSAEligibleIndicator>
            <ChildOnlyIndicator>true</ChildOnlyIndicator>
            <SameSexPartnerIndicator>false</SameSexPartnerIndicator>
            <DomesticPartnerIndicator>false</DomesticPartnerIndicator>
            <MaternityCoverageIndicator>true</MaternityCoverageIndicator>
            <MentalCoverageIndicator>true</MentalCoverageIndicator>
            <SubstanceAbuseCoverageIndicator>true</SubstanceAbuseCoverageIndicator>
            <DrugCoverageIndicator>true</DrugCoverageIndicator>
            <DentalCoverageIndicator>true</DentalCoverageIndicator>
            <VisionCoverageIndicator>true</VisionCoverageIndicator>
            <WellnessPorgramCoverageIndicator>false</WellnessPorgramCoverageIndicator>
            <FamilyAnnualDeductibleAmount>
                <Amount>42.00</Amount>
            </FamilyAnnualDeductibleAmount>
            <IndividualAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualDeductibleAmount>
            <FamilyAnnualOOPLimitAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </FamilyAnnualOOPLimitAmount>
            <IndividualAnnualOOPLimitAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualOOPLimitAmount>
        </ns2:Plan>
    </ns2:Plans>
    <ns2:PlanQuoteRequest>
        <ns2:Enrollees>
            <DateOfBirth>1959-03-03T00:00:00-06:00</DateOfBirth>
            <Gender>Male</Gender>
            <TobaccoLastUsedMonths>2</TobaccoLastUsedMonths>
            <Relation>SELF</Relation>
            <InHouseholdIndicator>true</InHouseholdIndicator>
        </ns2:Enrollees>
        <ns2:Enrollees>
            <DateOfBirth>1982-03-03T00:00:00-06:00</DateOfBirth>
            <Gender>Female</Gender>
            <TobaccoLastUsedMonths>2</TobaccoLastUsedMonths>
            <Relation>SPOUSE</Relation>
            <InHouseholdIndicator>true</InHouseholdIndicator>
        </ns2:Enrollees>
        <ns2:Location>
            <ZipCode>22901</ZipCode>
            <County>
                <FipsCode>51003</FipsCode>
                <CountyName>ALBEMARLE</CountyName>
                <StateCode>VA</StateCode>
            </County>
        </ns2:Location>
        <ns2:InsuranceEffectiveDate>2014-10-01T00:00:00-05:00</ns2:InsuranceEffectiveDate>
        <ns2:Market>Individual</ns2:Market>
        <ns2:IsFilterAnalysisRequiredIndicator>false</ns2:IsFilterAnalysisRequiredIndicator>
        <ns2:PaginationInformation>
            <PageNumber>1</PageNumber>
            <PageSize>20</PageSize>
        </ns2:PaginationInformation>
        <ns2:SortOrder>
            <SortField>BASE RATE</SortField>
            <SortDirection>ASC</SortDirection>
        </ns2:SortOrder>
    </ns2:PlanQuoteRequest>
</ns2:PlanQuoteResponse>'

$test_response_data_benefit = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns2:PlanBenefitResponse xmlns:ns2="http://hios.cms.org/api" xmlns="http://hios.cms.org/api-types">
    <ns2:ResponseHeader>
        <ReturnCode>Success</ReturnCode>
    </ns2:ResponseHeader>
    <ns2:PlanBenefits>
        <ns2:PlanBenefit>
            <PlanID>18628FL0150001</PlanID>
            <PlanNameText>CoreMed - Platinum 1 $950, 100%</PlanNameText>
            <ProductID>18628FL015</ProductID>
            <ProductNameText>2014 EHB IM PLAN</ProductNameText>
            <IssuerID>18628</IssuerID>
            <IssuerNameText>Aetna Health Inc.  (a FL corp.)</IssuerNameText>
            <IssuerStateCode>FL</IssuerStateCode>
            <IssuerTollFreeNumber>18773363915</IssuerTollFreeNumber>
            <IssuerLocalNumber>18773363915</IssuerLocalNumber>
            <IssuerTTYNumber xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <ProviderType>POS</ProviderType>
            <BaseRateAmount>116.00</BaseRateAmount>
            <MetalTierType>Bronze</MetalTierType>
            <HSAEligibleIndicator>false</HSAEligibleIndicator>
            <ChildOnlyIndicator>true</ChildOnlyIndicator>
            <SameSexPartnerIndicator>false</SameSexPartnerIndicator>
            <DomesticPartnerIndicator>false</DomesticPartnerIndicator>
            <MaternityCoverageIndicator>true</MaternityCoverageIndicator>
            <MentalCoverageIndicator>true</MentalCoverageIndicator>
            <SubstanceAbuseCoverageIndicator>true</SubstanceAbuseCoverageIndicator>
            <DrugCoverageIndicator>true</DrugCoverageIndicator>
            <DentalCoverageIndicator>true</DentalCoverageIndicator>
            <VisionCoverageIndicator>true</VisionCoverageIndicator>
            <WellnessPorgramCoverageIndicator>false</WellnessPorgramCoverageIndicator>
            <FamilyAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </FamilyAnnualDeductibleAmount>
            <IndividualAnnualDeductibleAmount>
                <NotApplicable>Not Applicable</NotApplicable>
            </IndividualAnnualDeductibleAmount>
            <FamilyAnnualOOPLimitAmount>
                <Amount>20.00</Amount>
            </FamilyAnnualOOPLimitAmount>
            <IndividualAnnualOOPLimitAmount>
                <Amount>10.00</Amount>
            </IndividualAnnualOOPLimitAmount>
            <IssuerURL>
                <URLCode>Valid</URLCode>
                <URLAddress>http://healthinsurance.aetna.com/</URLAddress>
                <AlternateText xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            </IssuerURL>
            <ProviderURL>
                <URLCode>Valid</URLCode>
                <URLAddress>http://www.provider1.com</URLAddress>
                <AlternateText>URL Pending Quality Evaluation</AlternateText>
            </ProviderURL>
            <PlanSummaryBenefitURL>
                <URLCode>Not Valid</URLCode>
                <URLAddress>www.healthSumBenTest31.com</URLAddress>
                <AlternateText>Incorrect link provided</AlternateText>
            </PlanSummaryBenefitURL>
            <PlanBrochureURL>
                <URLCode>Valid</URLCode>
                <URLAddress>http://www.benefits1.com</URLAddress>
                <AlternateText xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            </PlanBrochureURL>
            <DiseaseManagementProgramsOfferedText>High Blood Pressure & High Cholesterol, Low Back Pain</DiseaseManagementProgramsOfferedText>
            <OutOfServiceAreaCoverageIndicator>false</OutOfServiceAreaCoverageIndicator>
            <OutOfServiceAreaCoverageIndicatorText></OutOfServiceAreaCoverageIndicatorText>
            <OutOfCountryCoverageIndicator>false</OutOfCountryCoverageIndicator>
            <OutOfCountryCoverageIndicatorText></OutOfCountryCoverageIndicatorText>
            <NationalNetworkIndicator>false</NationalNetworkIndicator>
            <BasicDentalAdultCoverageIndicator>false</BasicDentalAdultCoverageIndicator>
            <BasicDentalChildCoverageIndicator>true</BasicDentalChildCoverageIndicator>
            <RoutineDentalAdultCoverageIndicator>false</RoutineDentalAdultCoverageIndicator>
            <RoutineDentalChildCoverageIndicator>true</RoutineDentalChildCoverageIndicator>
            <MajorDentalAdultCoverageIndicator>false</MajorDentalAdultCoverageIndicator>
            <MajorDentalChildCoverageIndicator>true</MajorDentalChildCoverageIndicator>
            <OrthodontiaAdultCoverageIndicator>false</OrthodontiaAdultCoverageIndicator>
            <OrthodontiaChildCoverageIndicator>true</OrthodontiaChildCoverageIndicator>
            <RoutineEyeExamAdultCoverageIndicator>false</RoutineEyeExamAdultCoverageIndicator>
            <RoutineEyeExamChildCoverageIndicator>true</RoutineEyeExamChildCoverageIndicator>
            <EyeGlassesChildrenCoverageIndicator>true</EyeGlassesChildrenCoverageIndicator>
            <PrimaryCareVisit>
                <CoPayInNetworkTier1>
                    <Benefit>Copay</Benefit>
                    <Amount>10.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>1</Percent>
                </CoInsuranceInNetworkTier1>
            </PrimaryCareVisit>
            <SpecialistVisit>
                <CoPayInNetworkTier1>
                    <Benefit>Copay after deductible</Benefit>
                    <Amount>13.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance after deductible</Benefit>
                    <Percent>4</Percent>
                </CoInsuranceInNetworkTier1>
            </SpecialistVisit>
            <ReferralRequiredSpecialistIndicator>false</ReferralRequiredSpecialistIndicator>
            <ReferralRequiredSpecialistText xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            <Imaging>
                <CoPayInNetworkTier1>
                    <Benefit>Copay before deductible</Benefit>
                    <Amount>16.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoInsuranceInNetworkTier1>
            </Imaging>
            <XRayDiagnosticImaging>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>3</Percent>
                </CoInsuranceInNetworkTier1>
            </XRayDiagnosticImaging>
            <LaboratoryOutPatientProfessionalServices>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoInsuranceInNetworkTier1>
            </LaboratoryOutPatientProfessionalServices>
            <InpatientHospitalServices>
                <CoPayInNetworkTier1>
                    <Benefit>Copay after deductible</Benefit>
                    <Amount>13.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance after deductible</Benefit>
                    <Percent>4</Percent>
                </CoInsuranceInNetworkTier1>
            </InpatientHospitalServices>
            <InpatientPhysicianAndSurgicalService>
                <CoPayInNetworkTier1>
                    <Benefit>Copay before deductible</Benefit>
                    <Amount>16.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoInsuranceInNetworkTier1>
            </InpatientPhysicianAndSurgicalService>
            <MaximumNumberOfDaysChargingInpatientCopay>0</MaximumNumberOfDaysChargingInpatientCopay>
            <EmergencyRoomServices>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>3</Percent>
                </CoInsuranceInNetworkTier1>
            </EmergencyRoomServices>
            <EmergencyTransportationAmbulanceService>
                <CoPayInNetworkTier1>
                    <Benefit>Copay</Benefit>
                    <Amount>10.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>1</Percent>
                </CoInsuranceInNetworkTier1>
            </EmergencyTransportationAmbulanceService>
            <GenericDrugs>
                <CoPayInNetworkTier1>
                    <Benefit>Copay</Benefit>
                    <Amount>10.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>1</Percent>
                </CoInsuranceInNetworkTier1>
            </GenericDrugs>
            <PreferredBrandDrugs>
                <CoPayInNetworkTier1>
                    <Benefit>Copay after deductible</Benefit>
                    <Amount>13.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance after deductible</Benefit>
                    <Percent>4</Percent>
                </CoInsuranceInNetworkTier1>
            </PreferredBrandDrugs>
            <NonPreferredBrandDrugs>
                <CoPayInNetworkTier1>
                    <Benefit>Copay before deductible</Benefit>
                    <Amount>16.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoInsuranceInNetworkTier1>
            </NonPreferredBrandDrugs>
            <SpecialityDrugs>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoInsuranceInNetworkTier1>
            </SpecialityDrugs>
            <MentalHealthOutPatientServices>
                <CoPayInNetworkTier1>
                    <Benefit>Copay after deductible</Benefit>
                    <Amount>13.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance after deductible</Benefit>
                    <Percent>4</Percent>
                </CoInsuranceInNetworkTier1>
            </MentalHealthOutPatientServices>
            <MentalHealthIntPatientServices>
                <CoPayInNetworkTier1>
                    <Benefit>Copay before deductible</Benefit>
                    <Amount>16.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoInsuranceInNetworkTier1>
            </MentalHealthIntPatientServices>
            <SubstanceAbuseDisorderOutPatientServices>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoInsuranceInNetworkTier1>
            </SubstanceAbuseDisorderOutPatientServices>
            <SubstanceAbuseDisorderInPatientServices>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>3</Percent>
                </CoInsuranceInNetworkTier1>
            </SubstanceAbuseDisorderInPatientServices>
            <NoticeRequiredForPregnancyIndicator>false</NoticeRequiredForPregnancyIndicator>
            <PrenatalAndPostnatalCare>
                <CoPayInNetworkTier1>
                    <Benefit>No Charge after deductible</Benefit>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>3</Percent>
                </CoInsuranceInNetworkTier1>
            </PrenatalAndPostnatalCare>
            <DeliveryAndAllInpatientServicesMaternityCare>
                <CoPayInNetworkTier1>
                    <Benefit>Copay</Benefit>
                    <Amount>10.00</Amount>
                </CoPayInNetworkTier1>
                <CoInsuranceInNetworkTier1>
                    <Benefit>Coinsurance</Benefit>
                    <Percent>1</Percent>
                </CoInsuranceInNetworkTier1>
            </DeliveryAndAllInpatientServicesMaternityCare>
            <IncludedBenefits>
                <BenefitName>Other Practitioner Office Visit (Nurse, Physician Assistant)</BenefitName>
                <BenefitName>Generic Drugs</BenefitName>
                <BenefitName>Preferred Brand Drugs</BenefitName>
                <BenefitName>Non-Preferred Brand Drugs</BenefitName>
                <BenefitName>Specialty Drugs</BenefitName>
                <BenefitName>Preventive Care/Screening/Immunization</BenefitName>
                <BenefitName>Primary Care Visit to Treat an Injury or Illness</BenefitName>
                <BenefitName>Specialist Visit</BenefitName>
                <BenefitName>Imaging (CT/PET Scans, MRIs)</BenefitName>
                <BenefitName>Outpatient Facility Fee (e.g.,  Ambulatory Surgery Center)</BenefitName>
                <BenefitName>Outpatient Surgery Physician/Surgical Services</BenefitName>
                <BenefitName>Emergency Room Services</BenefitName>
                <BenefitName>Emergency Transportation/Ambulance</BenefitName>
                <BenefitName>Urgent Care Centers or Facilities</BenefitName>
                <BenefitName>Inpatient Hospital Services (e.g., Hospital Stay)</BenefitName>
                <BenefitName>Inpatient Physician and Surgical Services</BenefitName>
                <BenefitName>Substance Abuse Disorder Outpatient Services</BenefitName>
                <BenefitName>Substance Abuse Disorder Inpatient Services</BenefitName>
                <BenefitName>Prenatal and Postnatal Care</BenefitName>
                <BenefitName>Delivery and All Inpatient Services for Maternity Care</BenefitName>
                <BenefitName>Home Health Care Services</BenefitName>
                <BenefitName>Durable Medical Equipment</BenefitName>
                <BenefitName>Hospice Services</BenefitName>
                <BenefitName>Routine Foot Care</BenefitName>
                <BenefitName>Non-Emergency Care When Traveling Outside the U.S.</BenefitName>
                <BenefitName>Basic Dental Care - Child</BenefitName>
                <BenefitName>Dental Anesthesia</BenefitName>
                <BenefitName>Diabetes Care Management</BenefitName>
                <BenefitName>Major Dental Care - Child</BenefitName>
                <BenefitName>Orthodontia - Child</BenefitName>
                <BenefitName>Accidental Dental</BenefitName>
                <BenefitName>Chemotherapy</BenefitName>
                <BenefitName>Laboratory Outpatient and Professional Services</BenefitName>
                <BenefitName>Reconstructive Surgery</BenefitName>
                <BenefitName>Transplant</BenefitName>
                <BenefitName>Treatment for Temporomandibular Joint Disorders</BenefitName>
                <BenefitName>X-rays and Diagnostic Imaging</BenefitName>
                <BenefitName>Bone Marrow Transplant</BenefitName>
                <BenefitName>Congenital Anomaly, including Cleft Lip/Palate</BenefitName>
                <BenefitName>Nutrition/Formulas</BenefitName>
                <BenefitName>Off Label Prescription Drugs</BenefitName>
                <BenefitName>Osteoporosis</BenefitName>
            </IncludedBenefits>
            <ExcludedBenefits>
                <BenefitName>Acupuncture</BenefitName>
                <BenefitName>Bariatric Surgery</BenefitName>
                <BenefitName>Cosmetic Surgery</BenefitName>
                <BenefitName>Hearing Aids</BenefitName>
                <BenefitName>Infertility Treatment</BenefitName>
                <BenefitName>Long-Term/Custodial Nursing Home Care</BenefitName>
                <BenefitName>Private-Duty Nursing</BenefitName>
                <BenefitName>Weight Loss Programs</BenefitName>
                <BenefitName>Habilitation Services</BenefitName>
                <BenefitName>Routine Dental Services (Adult)</BenefitName>
                <BenefitName>Routine Eye Exam (Adult)</BenefitName>
                <BenefitName>Basic Dental Care - Adult</BenefitName>
                <BenefitName>Major Dental Care - Adult</BenefitName>
                <BenefitName>Orthodontia - Adult</BenefitName>
                <BenefitName>Abortion for Which Public Funding is Prohibited</BenefitName>
                <BenefitName>Allergy Testing</BenefitName>
                <BenefitName>Diabetes Education</BenefitName>
                <BenefitName>Dialysis</BenefitName>
                <BenefitName>Infusion Therapy</BenefitName>
                <BenefitName>Nutritional Counseling</BenefitName>
                <BenefitName>Prosthetic Devices</BenefitName>
                <BenefitName>Radiation</BenefitName>
                <BenefitName>Rehabilitative Occupational and Rehabilitative Physical Therapy</BenefitName>
                <BenefitName>Rehabilitative Speech Therapy</BenefitName>
                <BenefitName>Well Baby Visits and Care</BenefitName>
            </ExcludedBenefits>
            <LimitedBenefits>
                <BenefitName>Skilled Nursing Facility</BenefitName>
                <BenefitName>Mental/Behavioral Health Outpatient Services</BenefitName>
                <BenefitName>Mental/Behavioral Health Inpatient Services</BenefitName>
                <BenefitName>Outpatient Rehabilitation Services</BenefitName>
                <BenefitName>Routine Eye Exam for Children</BenefitName>
                <BenefitName>Eye Glasses for Children</BenefitName>
                <BenefitName>Dental Check-Up for Children</BenefitName>
                <BenefitName>Chiropractic Care</BenefitName>
            </LimitedBenefits>
        </ns2:PlanBenefit>
    </ns2:PlanBenefits>
    <ns2:PlanBenefitRequest>
        <ns2:Enrollees>
            <DateOfBirth>1980-03-03T00:00:00-06:00</DateOfBirth>
            <Gender>Male</Gender>
            <TobaccoLastUsedMonths>2</TobaccoLastUsedMonths>
            <Relation>SELF</Relation>
            <InHouseholdIndicator>true</InHouseholdIndicator>
        </ns2:Enrollees>
        <ns2:Location>
            <ZipCode>32343</ZipCode>
            <County>
                <FipsCode>12039</FipsCode>
                <CountyName>GADSDEN</CountyName>
                <StateCode>FL</StateCode>
            </County>
        </ns2:Location>
        <ns2:InsuranceEffectiveDate>2014-10-01T00:00:00-05:00</ns2:InsuranceEffectiveDate>
        <ns2:Market>Individual</ns2:Market>
        <ns2:PlanIds>
            <ns2:PlanId>18628FL0150001</ns2:PlanId>
        </ns2:PlanIds>
    </ns2:PlanBenefitRequest>
</ns2:PlanBenefitResponse>'