class CreateIfpPlanQuotes < ActiveRecord::Migration
  def change

    create_table :ifp_plan_quotes do |t|

    	t.string :plan_id
    	t.string :plan_name_text
    	t.string :product_name_text
    	t.string :issuer_id
    	t.string :issuer_name_text
    	t.string :issuer_state_code
    	t.string :provider_type
    	t.string :base_rate_amount
    	t.string :metal_tier_type
    	t.string :child_only_indicator
    	t.string :hsa_eligible_indicator
    	t.string :same_sex_partner
    	t.string :domestic_partner
    	t.string :maternity_coverage_indicator
    	t.string :mental_coverage_indicator
    	t.string :prescription_in_network_coverage_indicator
    	t.string :substance_abuse_coverage_indicator
    	t.string :drug_coverage_indicator
    	t.string :dental_coverage_indicator
    	t.string :vision_coverage_indicator
    	t.string :wellness_program_coverage_indicator
    	t.string :zipcode
    	t.string :effective_date
        t.string :age
        t.string :gender_type
        t.string :tobacco_user
        t.timestamps

    end
    
  end
end

