class CreateIfpPlanBenefits < ActiveRecord::Migration
  def change
    create_table :ifp_plan_benefits do |t|
        

			t.string :plan_id
			t.string :family_deductible_amount
			t.string :individual_deductile_amount
			t.string :family_annual_OOP_limit_amount
			t.string :individual_annual_OOP_limit_amount
			t.string :issuer_URL_code
			t.string :issuer_URL_address
			t.string :provider_URL_code
			t.string :provider_URL_address
			t.string :product_benefit_URL_code
			t.string :product_benefit_URL_address


			t.string :plan_summary_URL_code
			t.string :plan_summary_URL_address
			t.string :plan_brochure_URL_code
			t.string :plan_brochure_URL_address
			t.string :disease_management_programs_offered_text


			t.string :primary_care_visit_co_pay_in_network_tier1_benefit
			t.string :primary_care_visit_co_pay_in_network_tier1_amount
			t.string :primary_care_visit_co_insurance_in_network_tier1_benefit
			t.string :primary_care_visit_co_insurance_in_network_tier1_percent


			t.string :specialist_visit_co_pay_in_network_tier1_benefit
			t.string :specialist_visit_co_pay_in_network_tier1_amount
			t.string :specialist_visit_co_insurance_in_network_tier1_benefit
			t.string :specialist_visit_co_insurance_in_network_tier1_percent

			t.string :referral_required_specialist_indicator


			t.string :imaging_co_pay_in_network_tier1_benefit
			t.string :imaging_co_pay_in_network_tier1_amount
			t.string :imaging_co_insurance_in_network_tier1_benefit

			t.string :xray_diagnostic_imaging_co_pay_in_network_tier1_benefit
			t.string :xray_diagnostic_imaging_co_insurance_in_network_tier1_benefit
			t.string :xray_diagnostic_imaging_co_insurance_in_network_tier1_percent

			t.string :laboratory_outpatient_co_pay_in_network_tier1_benefit
			t.string :laboratory_outpatient_co_insurance_in_network_tier1_benefit

			t.string :inpatient_hospital_co_pay_in_network_tier1_benefit
		  t.string :inpatient_hospital_co_pay_in_network_tier1_amount
			t.string :inpatient_hospital_co_insurance_in_network_tier1_benefit
		  t.string :inpatient_hospital_co_insurance_in_network_tier1_percent

			t.string :inpatient_physician_co_pay_in_network_tier1_benefit
		  t.string :inpatient_physician_co_pay_in_network_tier1_amount
			t.string :inpatient_physician_co_insurance_in_network_tier1_benefit

			t.string :maximum_numberofdays_charging_inpatient_copay
			t.string :emergency_room_services_co_pay_in_network_tier1_benefit
			t.string :emergency_room_services_co_insurance_in_network_tier1_benefit
			t.string :emergency_room_services_co_insurance_in_network_tier1_percent

			t.string :emergency_transportation_co_pay_in_network_tier1_benefit
			t.string :emergency_transportation_co_pay_in_network_tier1_amount
			t.string :emergency_transportation_co_insurance_in_network_tier1_benefit
			t.string :emergency_transportation_co_insurance_in_network_tier1_percent    	

			t.string :generic_drugs_co_pay_in_network_tier1_benefit
			t.string :generic_drugs_co_pay_in_network_tier1_amount
			t.string :generic_drugs_co_insurance_in_network_tier1_benefit
			t.string :generic_drugs_co_insurance_in_network_tier1_percent

			t.string :preferred_brand_drugs_co_pay_in_network_tier1_benefit
			t.string :preferred_brand_drugs_co_pay_in_network_tier1_amount
			t.string :preferred_brand_drugs_co_insurance_in_network_tier1_benefit
			t.string :preferred_brand_drugs_co_insurance_in_network_tier1_percent



			t.string :non_preferred_brand_drugs_co_pay_in_network_tier1_benefit
		  t.string :non_preferred_brand_drugs_co_pay_in_network_amount
		  t.string :non_preferred_brand_drugs_co_insurance_in_network_tier1_benefit
		  
			t.string :speciality_drugs_co_pay_in_network_benefit
			t.string :speciality_drugs_co_insurance_in_network_benefit

			t.string :mental_health_outpatient_co_pay_in_network_tier1_benefit
			t.string :mental_health_outpatient_co_pay_in_network_tier1_amount
			t.string :mental_health_outpatient_co_insurance_in_network_tier1_benefit
			t.string :mental_health_outpatient_co_insurance_in_network_tier1_percent

			t.string :mental_health_inpatient_co_pay_in_network_tier1_benefit
			t.string :mental_health_inpatient_co_pay_in_network_tier1_amount
			t.string :mental_health_inpatient_co_insurance_in_network_tier1_benefit

			t.string :notice_required_for_pregnancy_indicator

			t.string :prenatal_postnatal_care_co_pay_in_network_tier1_benefit
			t.string :prenatal_postnatal_care_co_insurance_in_network_tier1_benefit
			t.string :prenatal_postnatal_care_co_insurance_in_network_tier1_percent 

		  t.string :delivery_all_inpatient_co_pay_in_network_tier1_benefit
	    t.string :delivery_all_inpatient_co_pay_in_network_tier1_amount
	    t.string :delivery_all_inpatient_co_insurance_in_network_tier1_benefit
	    t.string :delivery_all_inpatient_co_insurance_in_network_tier1_percent

			t.string :included_benefit_name   	
			t.string :excluded_benefit_name
			t.string :limited_benefit_name
		  t.timestamps
    end
  end
end
