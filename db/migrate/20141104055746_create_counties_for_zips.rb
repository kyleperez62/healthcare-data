class CreateCountiesForZips < ActiveRecord::Migration
  def change
    create_table :counties_for_zips do |t|
      

    	t.string :zip_code
    	t.string :fips_code
    	t.string :county_name
    	t.string :state_code
    	
      t.timestamps

    end
  end
end
