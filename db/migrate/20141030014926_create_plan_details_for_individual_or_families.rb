class CreatePlanDetailsForIndividualOrFamilies < ActiveRecord::Migration
  def change
    create_table :plan_details_for_individual_or_families do |t|

        t.string :plan_id
        t.string :family_deductible_amount
        t.string :individual_deductile_amount
        t.string :PCP_copay_amount
        
        t.string :coinsurance_rate
        t.string :family_annual_OOP_limit_amount
        t.string :individual_annual_OOP_limit_amount
        t.string :annual_OOP_elements_text
        t.string :deductible_exceptions_services_text
        t.string :excluded_annual_OOP_limit_text
        t.string :issuer_URL_code
        t.string :issuer_URL_address
        t.string :provider_URL_code
        t.string :provider_URL_address
        t.string :product_benefit_URL_code
        t.string :product_benefit_URL_address
        t.string :product_formulary_URL_code
        t.string :product_formulary_URL_address
        t.string :plan_brochure_code
        t.string :plan_brochure_address
        t.string :other_practitioner_visit_in_network_benefit
        t.integer :other_practitioner_visit_in_network_percent
        t.string :primary_care_visit_in_network_benefit
        t.integer :primary_care_visit_in_network_percent
        t.string :primary_care_vist_in_network_limitation_type_text
        t.string :specialist_visit_in_network_benefit
        t.integer :specialist_visit_in_network_percent
        t.string :diagnostic_test_in_network_benefit
        t.integer :diagnostic_test_in_network_percent
        t.string :imaging_in_network_benefit
        t.integer :imaging_in_network_percent
        t.string :outpatient_facility_fee_in_network_benefit
        t.integer :outpatient_facility_fee_in_network_percent
        t.string :outpatient_physician_fee_in_network_benefit
        t.integer :outpatient_physician_fee_in_network_percent
        t.string :emergency_room_in_network_benefit
        t.integer :emergency_room_in_network_percent
        t.string :urgent_care_in_network_benefit
        t.integer :urgent_care_in_network_percent
        t.string :hospital_facility_fee_in_network_benefit
        t.integer :hospital_facility_fee_in_network_percent
        t.string :hospital_physician_fee_in_network_benefit
        t.integer :hospital_physician_fee_in_network_percent
        t.string :mental_outpatient_in_network_benefit
        t.integer :mental_outpatient_in_network_percent
        t.string :prenatal_postnatal_care_in_network_benefit
        t.integer :prenatal_postnatal_care_in_network_percent
        t.string :delivery_inpatient_in_network_benefit
        t.integer :delivery_inpatient_in_network_percent


        t.string :generic_drugs_retail_in_network_benefit
        t.integer :generic_drugs_retail_in_network_percent
        t.string :preferred_brand_drugs_retail_in_network_benefit
        t.integer :preferred_brand_drugs_retail_in_network_percent
        t.string :non_preferred_brand_drugs_retail_in_network_benefit
        t.integer :non_preferred_brand_drugs_retail_in_network_percent
        t.string :speciality_drugs_retail_in_network_benefit
        t.integer :speciality_drugs_retail_in_network_percent
        t.string :acupuncture
        t.string :bariatric_surgery
        t.string :non_emergecy_travel_outside
        t.string :chiropractic
        t.string :hearing_aid
        t.string :infertility_treatment
        t.string :long_term_care
        t.string :private_nursing
        t.string :eye_exam_adult
        t.string :routine_foot_care
        t.string :weight_loss_program
        t.string :routine_hearing_tests
        t.string :specialist_referral_required_services_text
        t.string :maternity_coverage_in_network_benefit
        t.integer :maternity_coverage_in_network_percent

        t.timestamps

    end
  end
end
