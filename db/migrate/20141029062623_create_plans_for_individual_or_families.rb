class CreatePlansForIndividualOrFamilies < ActiveRecord::Migration
  def change
    create_table :plans_for_individual_or_families do |t|
        
			t.string :plan_id
			t.string :plan_name_text
			t.string :product_name_text
			t.string :issuer_id
			t.string :issuer_name_text
			t.string :issuer_state_code
			t.string :provider_type
			t.float :base_rate_amount
			t.boolean :hsa_eligible_indicator
			t.boolean :same_sex_partner
			t.boolean :domestic_partner
	    t.string :zipcode
	    t.integer :age
	    t.string :gender_type
	    t.string :tobacco_user
        
      t.timestamps
      
    end
  end
end


