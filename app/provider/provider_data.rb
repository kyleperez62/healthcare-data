
require 'rest_client'
require 'nokogiri'
require 'open-uri'
require 'csv'


class ProviderData
	

	def extract_data(state_code,age_from,age_to,gender_type,tobacco_type)
		request_uri = "https://api.finder.healthcare.gov/v2.0/"

		#print "state:#{state_code}, from:#{age_from}, to:#{age_to}, sex:#{gender_type}, tobacco:#{tobacco_type}"
		page_size = 100;

		for state_key in $USA_zip_codes.keys
			if state_code == "ALL" || state_code == state_key
				for county_key in $USA_zip_codes[state_key].keys
					page_count = 1;
	        
	        for age_index in age_from..age_to

	          current_date = age_index.years.ago.beginning_of_year

	          request_data = "<?xml version='1.0' encoding='UTF-8'?>"
	          request_data += "<PrivateOptionsAPIRequest><PlansForIndividualOrFamilyRequest><Enrollees><Primary><DateOfBirth>"
	          request_data += current_date.strftime("%Y-%m-%d")
	          request_data += "</DateOfBirth><Gender>"
	          request_data += gender_type
	          request_data += "</Gender>"
	          request_data += "<TobaccoUser>"
	          request_data += tobacco_type
	          request_data += "</TobaccoUser></Primary></Enrollees><Location><ZipCode>"
	          request_data += $USA_zip_codes[state_key][county_key]
	          request_data += "</ZipCode><County><CountyName>"
	          request_data += county_key.upcase
	          request_data += "</CountyName><StateCode>"
	          request_data += state_key
	          request_data += "</StateCode></County></Location><InsuranceEffectiveDate>2014-11-01</InsuranceEffectiveDate>"
	          request_data += "<IsFilterAnalysisRequiredIndicator>false</IsFilterAnalysisRequiredIndicator><PaginationInformation><PageNumber>"
	          request_data += page_count.to_s
	          request_data += "</PageNumber><PageSize>"
	          request_data += page_size.to_s
	          request_data += "</PageSize></PaginationInformation><SortOrder><SortField>OOP LIMIT - INDIVIDUAL - IN NETWORK</SortField>
	            <SortDirection>ASC</SortDirection></SortOrder><Filter/></PlansForIndividualOrFamilyRequest></PrivateOptionsAPIRequest>"

						
	          begin
	            print "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	            #logger.debug "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	            
	            response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')

	            xml = Nokogiri::XML(response_data)
	            



	            total_plans_count = xml.search('TotalEligiblePlansQuanity').text.to_i;
	            if total_plans_count == 0
	            	next
	            end

	            plan_count = 0
	            plans = xml.search("Plan").map do |plan|
	              %w[
	                PlanID PlanNameText ProductNameText IssuerID IssuerNameText IssuerStateCode ProviderType BaseRateAmount HSAEligibleIndicator SameSexPartnerIndicator DomesticPartnerIndicator
	              ].each_with_object({}) do |n, o|
	                o[n] = plan.at(n).text
	              end
	            end

	            plan_count = plans.length
	            
	            for i in 0..plan_count - 1

	              plans_for_individual_or_family = PlansForIndividualOrFamily.new
	              plans_for_individual_or_family[:plan_id] = plans[i]["PlanID"]
	              plans_for_individual_or_family[:plan_name_text] = plans[i]["PlanNameText"]
	              plans_for_individual_or_family[:product_name_text] = plans[i]["ProductNameText"]
	              plans_for_individual_or_family[:issuer_id] = plans[i]["IssuerID"]
	              plans_for_individual_or_family[:issuer_name_text] = plans[i]["IssuerNameText"]
	              plans_for_individual_or_family[:issuer_state_code] = plans[i]["IssuerStateCode"]
	              plans_for_individual_or_family[:provider_type] = plans[i]["ProviderType"]
	              plans_for_individual_or_family[:base_rate_amount] = plans[i]["BaseRateAmount"].to_f
	              if plans[i]["HSAEligibleIndicator"] == "true"
	                plans_for_individual_or_family[:hsa_eligible_indicator] = true
	              else
	                plans_for_individual_or_family[:hsa_eligible_indicator] = false
	              end
	              
	              plans_for_individual_or_family[:same_sex_partner] = plans[i]["SameSexPartnerIndicator"]
	              plans_for_individual_or_family[:domestic_partner] = plans[i]["DomesticPartnerIndicator"]
	              plans_for_individual_or_family[:age] = age_index
	              plans_for_individual_or_family[:zipcode] = $USA_zip_codes[state_key][county_key]
	              plans_for_individual_or_family[:gender_type] = gender_type
	              plans_for_individual_or_family[:tobacco_user] = tobacco_type
	              
	              if PlansForIndividualOrFamily.exist_plan_id(plans[i]["PlanID"]).size == 0
	                plans_for_individual_or_family.save

	                plan_details_for_individual_or_family = PlanDetailsForIndividualOrFamily.new
	                request_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PrivateOptionsAPIRequest><PlanDetailsForIndividualOrFamilyRequest><Enrollees><Primary>"
	                request_data += "<DateOfBirth>"
	                request_data += current_date.strftime("%Y-%m-%d")
	                request_data += "</DateOfBirth><Gender>"
	                request_data += gender_type
	                request_data += "</Gender><TobaccoUser>"
	                request_data += tobacco_type
	                request_data += "</TobaccoUser></Primary></Enrollees><Location><ZipCode>"
	                request_data += $USA_zip_codes[state_key][county_key]
	                request_data += "</ZipCode><County><CountyName>"
	                request_data += county_key.upcase

	                request_data += "</CountyName><StateCode>"
	          			request_data += state_key
	          			request_data += "</StateCode></County></Location><InsuranceEffectiveDate>2014-11-01</InsuranceEffectiveDate>"
	                request_data += "<PlanIDs><PlanID>"
	                request_data += plans[i]["PlanID"]
	                request_data += "</PlanID></PlanIDs></PlanDetailsForIndividualOrFamilyRequest></PrivateOptionsAPIRequest>"

	                details_response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	                details_xml = Nokogiri::XML(details_response_data)
	                plan_details = details_xml.search("InNetworkCostSharing");
	                plan_details_for_individual_or_family[:plan_id] = plans[i]["PlanID"]
	                plan_details_for_individual_or_family[:family_deductible_amount] = plan_details.search("FamilyAnnualDeductibleAmount").first.text.strip
	                plan_details_for_individual_or_family[:individual_deductile_amount] = plan_details.search("IndividualAnnualDeductibleAmount").first.text.strip
	                plan_details_for_individual_or_family[:PCP_copay_amount] = plan_details.search("PCPCopayAmount").first.text.strip
	                plan_details_for_individual_or_family[:coinsurance_rate] = plan_details.search("CoinsuranceRate").first.text.strip
	                plan_details_for_individual_or_family[:family_annual_OOP_limit_amount] = plan_details.search("FamilyAnnualOOPLimitAmount").first.text.strip
	                plan_details_for_individual_or_family[:individual_annual_OOP_limit_amount] = plan_details.search("IndividualAnnualOOPLimitAmount").first.text.strip
	                plan_details_for_individual_or_family[:annual_OOP_elements_text] = plan_details.search("AnnualOOPElementsText").text
	                plan_details_for_individual_or_family[:excluded_annual_OOP_limit_text] = plan_details.search("ExcludedAnnualOOPLimitText").text
	                plan_details_for_individual_or_family[:deductible_exceptions_services_text] = plan_details.search("DeductibleExceptionsServicesText").text

	                plan_details = details_xml.search("IssuerURL");
	                plan_details_for_individual_or_family[:issuer_URL_code] = plan_details.search("URLCode").text
	                plan_details_for_individual_or_family[:issuer_URL_address] = plan_details.search("URLAddress").text

	                plan_details = details_xml.search("ProviderURL");
	                plan_details_for_individual_or_family[:provider_URL_code] = plan_details.search("URLCode").text
	                plan_details_for_individual_or_family[:provider_URL_address] = plan_details.search("URLAddress").text

	                plan_details = details_xml.search("ProductBenefitURL");
	                plan_details_for_individual_or_family[:product_benefit_URL_code] = plan_details.search("URLCode").text
	                plan_details_for_individual_or_family[:product_benefit_URL_address] = plan_details.search("URLAddress").text

	                plan_details = details_xml.search("ProductFormularyURL");
	                plan_details_for_individual_or_family[:product_formulary_URL_code] = plan_details.search("URLCode").text
	                plan_details_for_individual_or_family[:product_formulary_URL_address] = plan_details.search("URLAddress").text

	                plan_details = details_xml.search("PlanBrochureURL");
	                plan_details_for_individual_or_family[:plan_brochure_code] = plan_details.search("URLCode").text
	                plan_details_for_individual_or_family[:plan_brochure_address] = plan_details.search("URLAddress").text


	                plan_details = details_xml.search("OtherPractitionerVisit").search("InNetwork");
	                plan_details_for_individual_or_family[:other_practitioner_visit_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:other_practitioner_visit_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("PrimaryCareVisit").search("InNetwork");
	                plan_details_for_individual_or_family[:primary_care_visit_in_network_percent] = plan_details.search("Percent").text.to_i
	                plan_details_for_individual_or_family[:primary_care_visit_in_network_percent] = plan_details.search("Percent").text.to_i
	                plan_details_for_individual_or_family[:primary_care_vist_in_network_limitation_type_text] = ""

	                plan_details = details_xml.search("SpecialistVisit").search("InNetwork");
	                plan_details_for_individual_or_family[:specialist_visit_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:specialist_visit_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("DiagnosticTest").search("InNetwork");
	                plan_details_for_individual_or_family[:diagnostic_test_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:diagnostic_test_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("Imaging").search("InNetwork");
	                plan_details_for_individual_or_family[:imaging_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:imaging_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("OutpatientFacilityFee").search("InNetwork");
	                plan_details_for_individual_or_family[:outpatient_facility_fee_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:outpatient_facility_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("OutpatientPhysicianFee").search("InNetwork");
	                plan_details_for_individual_or_family[:outpatient_physician_fee_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:outpatient_physician_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("EmergencyRoom").search("InNetwork");
	                plan_details_for_individual_or_family[:emergency_room_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:emergency_room_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("UrgentCare").search("InNetwork");
	                plan_details_for_individual_or_family[:urgent_care_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:urgent_care_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("HospitalFacilityFee").search("InNetwork");
	                plan_details_for_individual_or_family[:hospital_facility_fee_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:hospital_facility_fee_in_network_percent] = plan_details.search("Percent").text.to_i


	                plan_details = details_xml.search("HospitalPhysicianFee").search("InNetwork");
	                plan_details_for_individual_or_family[:hospital_physician_fee_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:hospital_physician_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("MentalOutpatient").search("InNetwork");
	                plan_details_for_individual_or_family[:mental_outpatient_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:mental_outpatient_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("PrenatalPostnatalCare").search("InNetwork");
	                plan_details_for_individual_or_family[:prenatal_postnatal_care_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:prenatal_postnatal_care_in_network_percent] = plan_details.search("Percent").text.to_i


	                plan_details = details_xml.search("DeliveryInpatient").search("InNetwork");
	                plan_details_for_individual_or_family[:delivery_inpatient_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:delivery_inpatient_in_network_percent] = plan_details.search("Percent").text.to_i


	                plan_details = details_xml.search("GenericDrugsRetail").search("InNetwork");
	                plan_details_for_individual_or_family[:generic_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:generic_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("PreferredBrandDrugsRetail").search("InNetwork");
	                plan_details_for_individual_or_family[:preferred_brand_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:preferred_brand_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("NonPreferredBrandDrugsRetail").search("InNetwork");
	                plan_details_for_individual_or_family[:non_preferred_brand_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:non_preferred_brand_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details = details_xml.search("SpecialtyDrugsRetail").search("InNetwork");
	                plan_details_for_individual_or_family[:speciality_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                plan_details_for_individual_or_family[:speciality_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                plan_details_for_individual_or_family[:acupuncture] = details_xml.search("Acupuncture").text
	                plan_details_for_individual_or_family[:bariatric_surgery] = details_xml.search("BariatricSurgery").text
	                plan_details_for_individual_or_family[:non_emergecy_travel_outside] = details_xml.search("NonEmergencyTravelOutside").text
	                plan_details_for_individual_or_family[:chiropractic] = details_xml.search("Chiropractic").text
	                plan_details_for_individual_or_family[:hearing_aid] = details_xml.search("HearingAid").text
	                plan_details_for_individual_or_family[:infertility_treatment] = details_xml.search("InfertilityTreatment").text
	                plan_details_for_individual_or_family[:long_term_care] = details_xml.search("LongTermCare").text
	                plan_details_for_individual_or_family[:private_nursing] = details_xml.search("PrivateNursing").text
	                plan_details_for_individual_or_family[:eye_exam_adult] = details_xml.search("EyeExamAdult").text
	                plan_details_for_individual_or_family[:routine_foot_care] = details_xml.search("RoutineFootCare").text
	                plan_details_for_individual_or_family[:weight_loss_program] = details_xml.search("WeightLossProgram").text
	                plan_details_for_individual_or_family[:routine_hearing_tests] = details_xml.search("RoutineHearingTests").text
	                plan_details_for_individual_or_family[:specialist_referral_required_services_text] = details_xml.search("SpecialistReferralRequiredServicesText").text

	                plan_details_for_individual_or_family[:maternity_coverage_in_network_benefit] = ""
	                plan_details_for_individual_or_family[:maternity_coverage_in_network_percent] = 0

	                plan_details_for_individual_or_family.save

	              end
	            end


	            page_count = total_plans_count / page_size
	            if total_plans_count % page_size != 0
	              page_count +=1
	            end
	            for j in 2..page_count

	              request_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <PrivateOptionsAPIRequest><CountiesForPostalCodeRequest><ZipCode>20110</ZipCode></CountiesForPostalCodeRequest></PrivateOptionsAPIRequest>"
	              request_data = "<?xml version='1.0' encoding='UTF-8'?>"
	              request_data += "<PrivateOptionsAPIRequest><PlansForIndividualOrFamilyRequest><Enrollees><Primary><DateOfBirth>"
	              request_data += current_date.strftime("%Y-%m-%d")
	              request_data += "</DateOfBirth><Gender>"
	              request_data += gender_type
	              request_data += "</Gender>"
	              request_data += "<TobaccoUser>"
	              request_data += tobacco_type
	              request_data += "</TobaccoUser></Primary></Enrollees><Location><ZipCode>"
	              request_data += $USA_zip_codes[state_key][county_key]
	              request_data += "</ZipCode><County><CountyName>"
	              request_data += county_key.upcase
	              request_data += "</CountyName><StateCode>"
	          		request_data += state_key
	          		request_data += "</StateCode></County></Location><InsuranceEffectiveDate>2014-11-01</InsuranceEffectiveDate>"
	              request_data += "<IsFilterAnalysisRequiredIndicator>false</IsFilterAnalysisRequiredIndicator><PaginationInformation><PageNumber>"
	              request_data += j.to_s
	              request_data += "</PageNumber><PageSize>"
	              request_data += page_size.to_s
	              request_data += "</PageSize></PaginationInformation><SortOrder><SortField>OOP LIMIT - INDIVIDUAL - IN NETWORK</SortField>
	                <SortDirection>ASC</SortDirection></SortOrder><Filter/></PlansForIndividualOrFamilyRequest></PrivateOptionsAPIRequest>"




	              response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	              xml = Nokogiri::XML(response_data)

	              total_plans_count = xml.search('TotalEligiblePlansQuanity').text;
	              
	              
	              plan_count = 0
	              plans = xml.search("Plan").map do |plan|
	                %w[
	                  PlanID PlanNameText ProductNameText IssuerID IssuerNameText IssuerStateCode ProviderType BaseRateAmount HSAEligibleIndicator SameSexPartnerIndicator DomesticPartnerIndicator
	                ].each_with_object({}) do |n, o|
	                  o[n] = plan.at(n).text
	                end
	              end

	              plan_count = plans.length

	              # @parse_xml = ""

	              
	              for i in 0..plan_count - 1
	                plans_for_individual_or_family = PlansForIndividualOrFamily.new();
	                plans_for_individual_or_family[:plan_id] = plans[i]["PlanID"]
	                plans_for_individual_or_family[:plan_name_text] = plans[i]["PlanNameText"]
	                plans_for_individual_or_family[:product_name_text] = plans[i]["ProductNameText"]
	                plans_for_individual_or_family[:issuer_id] = plans[i]["IssuerID"]
	                plans_for_individual_or_family[:issuer_name_text] = plans[i]["IssuerNameText"]
	                plans_for_individual_or_family[:issuer_state_code] = plans[i]["IssuerStateCode"]
	                plans_for_individual_or_family[:provider_type] = plans[i]["ProviderType"]
	                plans_for_individual_or_family[:base_rate_amount] = plans[i]["BaseRateAmount"].to_f
	                if plans[i]["HSAEligibleIndicator"] == "true"
	                  plans_for_individual_or_family[:hsa_eligible_indicator] = true
	                else
	                  plans_for_individual_or_family[:hsa_eligible_indicator] = false
	                end
	                
	                plans_for_individual_or_family[:same_sex_partner] = plans[i]["SameSexPartnerIndicator"]
	                plans_for_individual_or_family[:domestic_partner] = plans[i]["DomesticPartnerIndicator"]
	                plans_for_individual_or_family[:age] = age_index
	                plans_for_individual_or_family[:zipcode] = $USA_zip_codes[state_key][county_key]
	                plans_for_individual_or_family[:gender_type] = gender_type
	                plans_for_individual_or_family[:tobacco_user] = tobacco_type
	                # @parse_xml += plans[i].to_s
	                if PlansForIndividualOrFamily.exist_plan_id(plans[i]["PlanID"]).size == 0
	                  plans_for_individual_or_family.save

	                  plan_details_for_individual_or_family = PlanDetailsForIndividualOrFamily.new
	                  request_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PrivateOptionsAPIRequest><PlanDetailsForIndividualOrFamilyRequest><Enrollees><Primary>"
	                  request_data += "<DateOfBirth>"
	                  request_data += current_date.strftime("%Y-%m-%d")
	                  request_data += "</DateOfBirth><Gender>"
	                  request_data += gender_type
	                  request_data += "</Gender><TobaccoUser>"
	                  request_data += tobacco_type
	                  request_data += "</TobaccoUser></Primary></Enrollees><Location><ZipCode>"
	                  request_data += $USA_zip_codes[state_key][county_key]
	                  request_data += "</ZipCode><County><CountyName>"
	                  request_data += county_key.upcase
	                  request_data += "</CountyName><StateCode>"
	          				request_data += state_key
	          				request_data += "</StateCode></County></Location><InsuranceEffectiveDate>2014-11-01</InsuranceEffectiveDate>"
	                  request_data += "<PlanIDs><PlanID>"
	                  request_data += plans[i]["PlanID"]
	                  request_data += "</PlanID></PlanIDs></PlanDetailsForIndividualOrFamilyRequest></PrivateOptionsAPIRequest>"

	                  details_response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	                  details_xml = Nokogiri::XML(details_response_data)
	                  plan_details = details_xml.search("InNetworkCostSharing");
	                  plan_details_for_individual_or_family[:plan_id] = plans[i]["PlanID"]
	                  plan_details_for_individual_or_family[:family_deductible_amount] = plan_details.search("FamilyAnnualDeductibleAmount").first.text.strip
	                  plan_details_for_individual_or_family[:individual_deductile_amount] = plan_details.search("IndividualAnnualDeductibleAmount").first.text.strip
	                  plan_details_for_individual_or_family[:PCP_copay_amount] = plan_details.search("PCPCopayAmount").first.text.strip
	                  plan_details_for_individual_or_family[:coinsurance_rate] = plan_details.search("CoinsuranceRate").first.text.strip
	                  plan_details_for_individual_or_family[:family_annual_OOP_limit_amount] = plan_details.search("FamilyAnnualOOPLimitAmount").first.text.strip
	                  plan_details_for_individual_or_family[:individual_annual_OOP_limit_amount] = plan_details.search("IndividualAnnualOOPLimitAmount").first.text.strip
	                  plan_details_for_individual_or_family[:annual_OOP_elements_text] = plan_details.search("AnnualOOPElementsText").text
	                  plan_details_for_individual_or_family[:excluded_annual_OOP_limit_text] = plan_details.search("ExcludedAnnualOOPLimitText").text
	                  plan_details_for_individual_or_family[:deductible_exceptions_services_text] = plan_details.search("DeductibleExceptionsServicesText").text

	                  plan_details = details_xml.search("IssuerURL");
	                  plan_details_for_individual_or_family[:issuer_URL_code] = plan_details.search("URLCode").text
	                  plan_details_for_individual_or_family[:issuer_URL_address] = plan_details.search("URLAddress").text

	                  plan_details = details_xml.search("ProviderURL");
	                  plan_details_for_individual_or_family[:provider_URL_code] = plan_details.search("URLCode").text
	                  plan_details_for_individual_or_family[:provider_URL_address] = plan_details.search("URLAddress").text

	                  plan_details = details_xml.search("ProductBenefitURL");
	                  plan_details_for_individual_or_family[:product_benefit_URL_code] = plan_details.search("URLCode").text
	                  plan_details_for_individual_or_family[:product_benefit_URL_address] = plan_details.search("URLAddress").text

	                  plan_details = details_xml.search("ProductFormularyURL");
	                  plan_details_for_individual_or_family[:product_formulary_URL_code] = plan_details.search("URLCode").text
	                  plan_details_for_individual_or_family[:product_formulary_URL_address] = plan_details.search("URLAddress").text

	                  plan_details = details_xml.search("PlanBrochureURL");
	                  plan_details_for_individual_or_family[:plan_brochure_code] = plan_details.search("URLCode").text
	                  plan_details_for_individual_or_family[:plan_brochure_address] = plan_details.search("URLAddress").text


	                  plan_details = details_xml.search("OtherPractitionerVisit").search("InNetwork");
	                  plan_details_for_individual_or_family[:other_practitioner_visit_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:other_practitioner_visit_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("PrimaryCareVisit").search("InNetwork");
	                  plan_details_for_individual_or_family[:primary_care_visit_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:primary_care_visit_in_network_percent] = plan_details.search("Percent").text.to_i
	                  plan_details_for_individual_or_family[:primary_care_vist_in_network_limitation_type_text] = ""

	                  plan_details = details_xml.search("SpecialistVisit").search("InNetwork");
	                  plan_details_for_individual_or_family[:specialist_visit_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:specialist_visit_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("DiagnosticTest").search("InNetwork");
	                  plan_details_for_individual_or_family[:diagnostic_test_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:diagnostic_test_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("Imaging").search("InNetwork");
	                  plan_details_for_individual_or_family[:imaging_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:imaging_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("OutpatientFacilityFee").search("InNetwork");
	                  plan_details_for_individual_or_family[:outpatient_facility_fee_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:outpatient_facility_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("OutpatientPhysicianFee").search("InNetwork");
	                  plan_details_for_individual_or_family[:outpatient_physician_fee_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:outpatient_physician_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("EmergencyRoom").search("InNetwork");
	                  plan_details_for_individual_or_family[:emergency_room_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:emergency_room_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("UrgentCare").search("InNetwork");
	                  plan_details_for_individual_or_family[:urgent_care_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:urgent_care_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("HospitalFacilityFee").search("InNetwork");
	                  plan_details_for_individual_or_family[:hospital_facility_fee_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:hospital_facility_fee_in_network_percent] = plan_details.search("Percent").text.to_i


	                  plan_details = details_xml.search("HospitalPhysicianFee").search("InNetwork");
	                  plan_details_for_individual_or_family[:hospital_physician_fee_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:hospital_physician_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("MentalOutpatient").search("InNetwork");
	                  plan_details_for_individual_or_family[:mental_outpatient_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:mental_outpatient_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("PrenatalPostnatalCare").search("InNetwork");
	                  plan_details_for_individual_or_family[:prenatal_postnatal_care_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:prenatal_postnatal_care_in_network_percent] = plan_details.search("Percent").text.to_i


	                  plan_details = details_xml.search("DeliveryInpatient").search("InNetwork");
	                  plan_details_for_individual_or_family[:delivery_inpatient_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:delivery_inpatient_in_network_percent] = plan_details.search("Percent").text.to_i


	                  plan_details = details_xml.search("GenericDrugsRetail").search("InNetwork");
	                  plan_details_for_individual_or_family[:generic_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:generic_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("PreferredBrandDrugsRetail").search("InNetwork");
	                  plan_details_for_individual_or_family[:preferred_brand_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:preferred_brand_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("NonPreferredBrandDrugsRetail").search("InNetwork");
	                  plan_details_for_individual_or_family[:non_preferred_brand_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:non_preferred_brand_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details = details_xml.search("SpecialtyDrugsRetail").search("InNetwork");
	                  plan_details_for_individual_or_family[:speciality_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	                  plan_details_for_individual_or_family[:speciality_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	                  plan_details_for_individual_or_family[:acupuncture] = details_xml.search("Acupuncture").text
	                  plan_details_for_individual_or_family[:bariatric_surgery] = details_xml.search("BariatricSurgery").text
	                  plan_details_for_individual_or_family[:non_emergecy_travel_outside] = details_xml.search("NonEmergencyTravelOutside").text
	                  plan_details_for_individual_or_family[:chiropractic] = details_xml.search("Chiropractic").text
	                  plan_details_for_individual_or_family[:hearing_aid] = details_xml.search("HearingAid").text
	                  plan_details_for_individual_or_family[:infertility_treatment] = details_xml.search("InfertilityTreatment").text
	                  plan_details_for_individual_or_family[:long_term_care] = details_xml.search("LongTermCare").text
	                  plan_details_for_individual_or_family[:private_nursing] = details_xml.search("PrivateNursing").text
	                  plan_details_for_individual_or_family[:eye_exam_adult] = details_xml.search("EyeExamAdult").text
	                  plan_details_for_individual_or_family[:routine_foot_care] = details_xml.search("RoutineFootCare").text
	                  plan_details_for_individual_or_family[:weight_loss_program] = details_xml.search("WeightLossProgram").text
	                  plan_details_for_individual_or_family[:routine_hearing_tests] = details_xml.search("RoutineHearingTests").text
	                  plan_details_for_individual_or_family[:specialist_referral_required_services_text] = details_xml.search("SpecialistReferralRequiredServicesText").text

	                  plan_details_for_individual_or_family[:maternity_coverage_in_network_benefit] = ""
	                  plan_details_for_individual_or_family[:maternity_coverage_in_network_percent] = 0

	                  plan_details_for_individual_or_family.save
	                end
	              end

	            end      

	          rescue => ex
	            print "***********************error****************************"
	            print ex.message
	            #print "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	            #logger.debug "***********************error****************************"
	            #logger.debug "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	          end
	        end

	        
				end
				
			end
		end

	end

end