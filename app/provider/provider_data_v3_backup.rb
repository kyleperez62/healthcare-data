
require 'rest_client'
require 'nokogiri'
require 'open-uri'
require 'csv'


class ProviderDataV3Backup
	
	def extract_counties
		request_uri = "https://api.finder.healthcare.gov/v3.0/getCountiesForZip"
		# for state_key in $USA_zip_codes.keys
		# 	for county_key in $USA_zip_codes[state_key].keys
				
		# 	end
		# end


		CSV.foreach('Top5.csv') do |row|
		  #puts row[0]

		  zip_code = row[2]
				
			request_data = '<p:ZipCodeValidationRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
			request_data += '<p:ZipCode>'
			request_data += zip_code
			request_data += '</p:ZipCode></p:ZipCodeValidationRequest>'

			begin
				print "StateCode:" + row[0] + "--------ZipCode:" + row[1] 

				response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
				xml = Nokogiri::XML(response_data)

        county_for_zip = CountiesForZip.new
      	county_for_zip[:zip_code] = zip_code
      	county_for_zip[:fips_code] = xml.search('FipsCode')[0].text
      	county_for_zip[:county_name] = xml.search('CountyName')[0].text.upcase
      	county_for_zip[:state_code] = xml.search('StateCode')[0].text.upcase
      	if CountiesForZip.exist_zip_code(zip_code).size == 0
      		county_for_zip.save
      	end

			rescue Exception => ex
				print "***********************error****************************"
        print ex.message
        
			end
		end

	end
	
	def extract_data_v3(state_code,age_from,age_to,gender_type,tobacco_type)
		

		#print "state:#{state_code}, from:#{age_from}, to:#{age_to}, sex:#{gender_type}, tobacco:#{tobacco_type}"
		page_size = 100

		for county_for_zip in CountiesForZip.get_zip_code_by_state_code(state_code).limit(1)
			for age_index in age_from..age_to

				page_number = 1

				current_date = age_index.years.ago.beginning_of_year
	      request_uri = "https://api.finder.healthcare.gov/v3.0/getIFPPlanQuotes"

	      request_data = '<?xml version="1.0" encoding="UTF-8"?>'
	      request_data += '<p:PlanQuoteRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
	      request_data += '<p:Enrollees><p1:DateOfBirth>'
	      request_data += current_date.strftime("%Y-%m-%d")
	      request_data += '</p1:DateOfBirth><p1:Gender>'
	      request_data += gender_type
	      request_data += '</p1:Gender><p1:TobaccoLastUsedMonths>2'
	      request_data += '</p1:TobaccoLastUsedMonths><p1:Relation>SELF</p1:Relation><p1:InHouseholdIndicator>true</p1:InHouseholdIndicator></p:Enrollees>'
	      request_data += '<p:Location><p1:ZipCode>'
	      request_data += county_for_zip[:zip_code]
	      request_data += '</p1:ZipCode><p1:County>'
	      request_data += '<p1:FipsCode>'
	      request_data += county_for_zip[:fips_code]
	      request_data += '</p1:FipsCode>'
	      request_data += '<p1:CountyName>'
	      request_data += county_for_zip[:county_name]
	      request_data += '</p1:CountyName><p1:StateCode>'
	      request_data += county_for_zip[:state_code]
	      request_data += '</p1:StateCode></p1:County></p:Location>'
	      request_data += '<p:InsuranceEffectiveDate>2015-01-01</p:InsuranceEffectiveDate>'
	      request_data += '<p:Market>Individual</p:Market><p:IsFilterAnalysisRequiredIndicator>false</p:IsFilterAnalysisRequiredIndicator>'
	      request_data += '<p:PaginationInformation><p1:PageNumber>'
	      request_data += page_number.to_s
	      request_data += '</p1:PageNumber><p1:PageSize>'
	      request_data += page_size.to_s
	      request_data += '</p1:PageSize></p:PaginationInformation>'
	      request_data += '<p:SortOrder><p1:SortField>BASE RATE</p1:SortField><p1:SortDirection>ASC</p1:SortDirection></p:SortOrder></p:PlanQuoteRequest>'
	      
	      
	      begin
	        print "#{county_for_zip[:state_code]}-----#{county_for_zip[:county_name]}-----#{age_index.to_s}"
	        #logger.debug "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	        
	        #response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	        response_data = $test_response_data_quote
	        
	        xml = Nokogiri::XML(response_data)
	        plan_ids = xml.search("PlanID")

	        total_plans_count = plan_ids.size
	        if total_plans_count == 0
	        	#next
	        end

	        
	        plan_name_texts = xml.search("PlanNameText")
	        product_name_texts = xml.search("ProductNameText")
	        issuer_ids = xml.search("IssuerID")
	        issuer_name_texts = xml.search("IssuerNameText")
	        issuer_state_codes = xml.search("IssuerStateCode")
	        provider_types = xml.search("ProviderType")
	        base_rate_amounts = xml.search("BaseRateAmount")
	        metal_tier_types = xml.search("MetalTierType")
	        child_only_indicators = xml.search("ChildOnlyIndicator")
	        hsa_eligible_indicators = xml.search("HSAEligibleIndicator")
	        same_sex_partners = xml.search("SameSexPartnerIndicator")
	        domestic_partners = xml.search("DomesticPartnerIndicator")
	        maternity_coverage_indicators = xml.search("MaternityCoverageIndicator")
	        mental_coverage_indicators = xml.search("MentalCoverageIndicator")
	        #prescription_in_network_coverage_indicators = xml.search("PlanID")
	        substance_abuse_coverage_indicators = xml.search("SubstanceAbuseCoverageIndicator")
	        drug_coverage_indicators = xml.search("DrugCoverageIndicator")
	        dental_coverage_indicators = xml.search("DentalCoverageIndicator")
	        vision_coverage_indicators = xml.search("VisionCoverageIndicator")
	        wellness_program_coverage_indicators = xml.search("WellnessPorgramCoverageIndicator")

	        

	        for i in 0..total_plans_count - 1

	          ifp_plan_quote = IfpPlanQuote.new

	          
	          ifp_plan_quote[:plan_id] = plan_ids[i].text

	          ifp_plan_quote[:plan_name_text] = plan_name_texts[i].text
	          ifp_plan_quote[:product_name_text] = product_name_texts[i].text
	          ifp_plan_quote[:issuer_id] = issuer_ids[i].text
	          ifp_plan_quote[:issuer_name_text] = issuer_name_texts[i].text
	          ifp_plan_quote[:issuer_state_code] = issuer_state_codes[i].text
	          ifp_plan_quote[:provider_type] = provider_types[i].text
	          ifp_plan_quote[:base_rate_amount] = base_rate_amounts[i].text
	          ifp_plan_quote[:metal_tier_type] = metal_tier_types[i].text
	          ifp_plan_quote[:child_only_indicator] = child_only_indicators[i].text
	          ifp_plan_quote[:hsa_eligible_indicator] = hsa_eligible_indicators[i].text
	          ifp_plan_quote[:same_sex_partner] = same_sex_partners[i].text
	          ifp_plan_quote[:domestic_partner] = domestic_partners[i].text

	          

	          ifp_plan_quote[:maternity_coverage_indicator] = maternity_coverage_indicators[i].text
	          ifp_plan_quote[:mental_coverage_indicator] = mental_coverage_indicators[i].text
	          ifp_plan_quote[:prescription_in_network_coverage_indicator] = ''
	          ifp_plan_quote[:substance_abuse_coverage_indicator] = substance_abuse_coverage_indicators[i].text
	          ifp_plan_quote[:drug_coverage_indicator] = drug_coverage_indicators[i].text
	          ifp_plan_quote[:dental_coverage_indicator] = dental_coverage_indicators[i].text
	          ifp_plan_quote[:vision_coverage_indicator] = vision_coverage_indicators[i].text
	          ifp_plan_quote[:wellness_program_coverage_indicator] = wellness_program_coverage_indicators[i].text

	          ifp_plan_quote[:age] = age_index.to_s
	          ifp_plan_quote[:effective_date] = '2015-01-01'
	          ifp_plan_quote[:zipcode] = county_for_zip[:zip_code]
	          ifp_plan_quote[:gender_type] = gender_type
	          ifp_plan_quote[:tobacco_user] = ''

	          print 'ccc'
	          
	          if IfpPlanQuote.exist_plan_id(plan_ids[i].text).size != 0
	          	next
	          else

	          	#ifp_plan_quote.save

	          	request_uri = "https://api.finder.healthcare.gov/v3.0/getIFPPlanQuotes"

	          	request_data = '<?xml version="1.0" encoding="UTF-8"?>'
	          	request_data += '<p:PlanBenefitRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" 
	          		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
	          	request_data += '<p:Enrollees><p1:DateOfBirth>'
	          	request_data += current_date.strftime("%Y-%m-%d")
	          	request_data += '</p1:DateOfBirth><p1:Gender>'
	          	request_data += gender_type
	          	request_data += '</p1:Gender><p1:TobaccoLastUsedMonths>2</p1:TobaccoLastUsedMonths><p1:Relation>SELF</p1:Relation>'
	          	request_data += '<p1:InHouseholdIndicator>true</p1:InHouseholdIndicator></p:Enrollees>'
	          	request_data += '<p:Location><p1:ZipCode>'
	          	request_data += county_for_zip[:zip_code]
	          	request_data += '</p1:ZipCode><p1:County><p1:FipsCode>'
	          	request_data += county_for_zip[:fips_code]
	          	request_data += '</p1:FipsCode><p1:CountyName>'
	          	request_data += county_for_zip[:county_name]
	          	request_data += '</p1:CountyName><p1:StateCode>'
	          	request_data += county_for_zip[:state_code]
	          	request_data += '</p1:StateCode></p1:County></p:Location>'
	          	request_data += '<p:InsuranceEffectiveDate>2015-01-01</p:InsuranceEffectiveDate><p:Market>Individual</p:Market>'
	          	request_data += '<p:PlanIds><p:PlanId>'
	          	request_data += plan_ids[i].text
	          	request_data += '</p:PlanId></p:PlanIds></p:PlanBenefitRequest>'

	          	

	            details_response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	            details_response_data = $test_response_data_benefit

	            ifp_plan_benefit = IfpPlanBenefit.new
	            details_xml = Nokogiri::XML(details_response_data)

	            plan_details = details_xml.search("FamilyAnnualDeductibleAmount")

	            ifp_plan_benefit[:plan_id] = plan_ids[i].text
	            ifp_plan_benefit[:family_deductible_amount] = details_xml.search("FamilyAnnualDeductibleAmount").first.text.strip
	            ifp_plan_benefit[:individual_deductile_amount] = details_xml.search("IndividualAnnualDeductibleAmount").first.text.strip
	            ifp_plan_benefit[:family_annual_OOP_limit_amount] = details_xml.search("FamilyAnnualOOPLimitAmount").first.text.strip
	            ifp_plan_benefit[:individual_annual_OOP_limit_amount] = details_xml.search("IndividualAnnualOOPLimitAmount").first.text.strip
	            
	            plan_details = details_xml.search("IssuerURL")
	            ifp_plan_benefit[:issuer_URL_code] = plan_details.search("URLCode").text
	            ifp_plan_benefit[:issuer_URL_address] = plan_details.search("URLAddress").text

	            plan_details = details_xml.search("ProviderURL")
	            ifp_plan_benefit[:provider_URL_code] = plan_details.search("URLCode").text
	            ifp_plan_benefit[:provider_URL_address] = plan_details.search("URLAddress").text

	            plan_details = details_xml.search("ProductBenefitURL")
	            ifp_plan_benefit[:product_benefit_URL_code] = ''
	            ifp_plan_benefit[:product_benefit_URL_address] = ''

	            plan_details = details_xml.search("PlanSummaryBenefitURL")
	            ifp_plan_benefit[:plan_summary_URL_code] = plan_details.search("URLCode").text
	            ifp_plan_benefit[:plan_summary_URL_address] = plan_details.search("URLAddress").text

	            plan_details = details_xml.search("PlanBrochureURL")
	            ifp_plan_benefit[:plan_brochure_URL_code] = plan_details.search("URLCode").text
	            ifp_plan_benefit[:plan_brochure_URL_address] = plan_details.search("URLAddress").text

	            ifp_plan_benefit[:disease_management_programs_offered_text] = details_xml.search("DiseaseManagementProgramsOfferedText").text


	            
	            plan_details = details_xml.search("PrimaryCareVisit").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:primary_care_visit_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:primary_care_visit_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("PrimaryCareVisit").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:primary_care_visit_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:primary_care_visit_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("SpecialistVisit").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:specialist_visit_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:specialist_visit_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("SpecialistVisit").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:specialist_visit_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:specialist_visit_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            ifp_plan_benefit[:referral_required_specialist_indicator] = details_xml.search("ReferralRequiredSpecialistIndicator").text

	            plan_details = details_xml.search("Imaging").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:imaging_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:imaging_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("Imaging").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:imaging_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            
	            plan_details = details_xml.search("XRayDiagnosticImaging").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:xray_diagnostic_imaging_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            plan_details = details_xml.search("XRayDiagnosticImaging").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:xray_diagnostic_imaging_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:xray_diagnostic_imaging_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            
	            plan_details = details_xml.search("LaboratoryOutPatientProfessionalServices").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:laboratory_outpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            plan_details = details_xml.search("LaboratoryOutPatientProfessionalServices").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:laboratory_outpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text

	            plan_details = details_xml.search("InpatientHospitalServices").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:inpatient_hospital_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:inpatient_hospital_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("InpatientHospitalServices").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:inpatient_hospital_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:inpatient_hospital_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text.to_i

	            plan_details = details_xml.search("InpatientPhysicianAndSurgicalService").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:inpatient_physician_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:inpatient_physician_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("InpatientPhysicianAndSurgicalService").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:inpatient_physician_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text

	            ifp_plan_benefit[:maximum_numberofdays_charging_inpatient_copay] = details_xml.search("MaximumNumberOfDaysChargingInpatientCopay").text

	            plan_details = details_xml.search("EmergencyRoomServices").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:emergency_room_services_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            plan_details = details_xml.search("EmergencyRoomServices").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:emergency_room_services_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:emergency_room_services_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text
				            

	            plan_details = details_xml.search("EmergencyTransportationAmbulanceService").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:emergency_transportation_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:emergency_transportation_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("EmergencyTransportationAmbulanceService").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:emergency_transportation_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:emergency_transportation_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("GenericDrugs").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:generic_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:generic_drugs_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("GenericDrugs").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:generic_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:generic_drugs_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("PreferredBrandDrugs").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:preferred_brand_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:preferred_brand_drugs_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("PreferredBrandDrugs").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:preferred_brand_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:preferred_brand_drugs_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("NonPreferredBrandDrugs").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:non_preferred_brand_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:non_preferred_brand_drugs_co_pay_in_network_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("NonPreferredBrandDrugs").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:non_preferred_brand_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            
	            plan_details = details_xml.search("SpecialityDrugs").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:speciality_drugs_co_pay_in_network_benefit] = plan_details.search("Benefit").text
	            plan_details = details_xml.search("SpecialityDrugs").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:speciality_drugs_co_insurance_in_network_benefit] = plan_details.search("Benefit").text
	            
	            plan_details = details_xml.search("MentalHealthOutPatientServices").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:mental_health_outpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:mental_health_outpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("MentalHealthOutPatientServices").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("MentalHealthIntPatientServices").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:mental_health_inpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:mental_health_inpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("MentalHealthIntPatientServices").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:mental_health_inpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            ifp_plan_benefit[:notice_required_for_pregnancy_indicator] = details_xml.search("NoticeRequiredForPregnancyIndicator").text

	            plan_details = details_xml.search("PrenatalAndPostnatalCare").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:prenatal_postnatal_care_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            plan_details = details_xml.search("PrenatalAndPostnatalCare").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:prenatal_postnatal_care_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:prenatal_postnatal_care_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("DeliveryAndAllInpatientServicesMaternityCare").search("CoPayInNetworkTier1")
	            ifp_plan_benefit[:delivery_all_inpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:delivery_all_inpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	            plan_details = details_xml.search("DeliveryAndAllInpatientServicesMaternityCare").search("CoInsuranceInNetworkTier1")
	            ifp_plan_benefit[:delivery_all_inpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	            ifp_plan_benefit[:delivery_all_inpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	            plan_details = details_xml.search("IncludedBenefits")
	            ifp_plan_benefit[:included_benefit_name] = ''
	            ifp_plan_benefit[:excluded_benefit_name] = ''
	            ifp_plan_benefit[:limited_benefit_name] = ''

	            

	            ifp_plan_benefit.save

	          end
	        end
	        
	        return

	        page_number = total_plans_count / page_size
	        if total_plans_count % page_size != 0
	          page_number +=1
	        end
	        for j in 2..page_number

	          request_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <PrivateOptionsAPIRequest><CountiesForPostalCodeRequest><ZipCode>20110</ZipCode></CountiesForPostalCodeRequest></PrivateOptionsAPIRequest>"
	          request_data = "<?xml version='1.0' encoding='UTF-8'?>"
	          request_data += "<PrivateOptionsAPIRequest><PlansForIndividualOrFamilyRequest><Enrollees><Primary><DateOfBirth>"
	          request_data += current_date.strftime("%Y-%m-%d")
	          request_data += "</DateOfBirth><Gender>"
	          request_data += gender_type
	          request_data += "</Gender>"
	          request_data += "<TobaccoUser>"
	          request_data += tobacco_type
	          request_data += "</TobaccoUser></Primary></Enrollees><Location><ZipCode>"
	          request_data += $USA_zip_codes[state_key][county_key]
	          request_data += "</ZipCode><County><CountyName>"
	          request_data += county_key.upcase
	          request_data += "</CountyName><StateCode>"
	      		request_data += state_key
	      		request_data += "</StateCode></County></Location><InsuranceEffectiveDate>2014-11-01</InsuranceEffectiveDate>"
	          request_data += "<IsFilterAnalysisRequiredIndicator>false</IsFilterAnalysisRequiredIndicator><PaginationInformation><PageNumber>"
	          request_data += j.to_s
	          request_data += "</PageNumber><PageSize>"
	          request_data += page_size.to_s
	          request_data += "</PageSize></PaginationInformation><SortOrder><SortField>OOP LIMIT - INDIVIDUAL - IN NETWORK</SortField>
	            <SortDirection>ASC</SortDirection></SortOrder><Filter/></PlansForIndividualOrFamilyRequest></PrivateOptionsAPIRequest>"




	          response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	          xml = Nokogiri::XML(response_data)

	          total_plans_count = xml.search('TotalEligiblePlansQuanity').text
	          
	          
	          plan_count = 0
	          plans = xml.search("Plan").map do |plan|
	            %w[
	              PlanID PlanNameText ProductNameText IssuerID IssuerNameText IssuerStateCode ProviderType BaseRateAmount HSAEligibleIndicator SameSexPartnerIndicator DomesticPartnerIndicator
	            ].each_with_object({}) do |n, o|
	              o[n] = plan.at(n).text
	            end
	          end

	          plan_count = plans.length

	          # @parse_xml = ""

	          
	          for i in 0..plan_count - 1
	            plans_for_individual_or_family = PlansForIndividualOrFamily.new()
	            plans_for_individual_or_family[:plan_id] = plans[i]["PlanID"]
	            plans_for_individual_or_family[:plan_name_text] = plans[i]["PlanNameText"]
	            plans_for_individual_or_family[:product_name_text] = plans[i]["ProductNameText"]
	            plans_for_individual_or_family[:issuer_id] = plans[i]["IssuerID"]
	            plans_for_individual_or_family[:issuer_name_text] = plans[i]["IssuerNameText"]
	            plans_for_individual_or_family[:issuer_state_code] = plans[i]["IssuerStateCode"]
	            plans_for_individual_or_family[:provider_type] = plans[i]["ProviderType"]
	            plans_for_individual_or_family[:base_rate_amount] = plans[i]["BaseRateAmount"].to_f
	            if plans[i]["HSAEligibleIndicator"] == "true"
	              plans_for_individual_or_family[:hsa_eligible_indicator] = true
	            else
	              plans_for_individual_or_family[:hsa_eligible_indicator] = false
	            end
	            
	            plans_for_individual_or_family[:same_sex_partner] = plans[i]["SameSexPartnerIndicator"]
	            plans_for_individual_or_family[:domestic_partner] = plans[i]["DomesticPartnerIndicator"]
	            plans_for_individual_or_family[:age] = age_index
	            plans_for_individual_or_family[:zipcode] = $USA_zip_codes[state_key][county_key]
	            plans_for_individual_or_family[:gender_type] = gender_type
	            plans_for_individual_or_family[:tobacco_user] = tobacco_type
	            # @parse_xml += plans[i].to_s
	            if PlansForIndividualOrFamily.exist_plan_id(plans[i]["PlanID"]).size == 0
	              plans_for_individual_or_family.save

	              plan_details_for_individual_or_family = PlanDetailsForIndividualOrFamily.new
	              request_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PrivateOptionsAPIRequest><PlanDetailsForIndividualOrFamilyRequest><Enrollees><Primary>"
	              request_data += "<DateOfBirth>"
	              request_data += current_date.strftime("%Y-%m-%d")
	              request_data += "</DateOfBirth><Gender>"
	              request_data += gender_type
	              request_data += "</Gender><TobaccoUser>"
	              request_data += tobacco_type
	              request_data += "</TobaccoUser></Primary></Enrollees><Location><ZipCode>"
	              request_data += $USA_zip_codes[state_key][county_key]
	              request_data += "</ZipCode><County><CountyName>"
	              request_data += county_key.upcase
	              request_data += "</CountyName><StateCode>"
	      				request_data += state_key
	      				request_data += "</StateCode></County></Location><InsuranceEffectiveDate>2014-11-01</InsuranceEffectiveDate>"
	              request_data += "<PlanIDs><PlanID>"
	              request_data += plans[i]["PlanID"]
	              request_data += "</PlanID></PlanIDs></PlanDetailsForIndividualOrFamilyRequest></PrivateOptionsAPIRequest>"

	              details_response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	              details_xml = Nokogiri::XML(details_response_data)
	              plan_details = details_xml.search("InNetworkCostSharing")
	              plan_details_for_individual_or_family[:plan_id] = plans[i]["PlanID"]
	              plan_details_for_individual_or_family[:family_deductible_amount] = plan_details.search("FamilyAnnualDeductibleAmount").first.text.strip
	              plan_details_for_individual_or_family[:individual_deductile_amount] = plan_details.search("IndividualAnnualDeductibleAmount").first.text.strip
	              plan_details_for_individual_or_family[:PCP_copay_amount] = plan_details.search("PCPCopayAmount").first.text.strip
	              plan_details_for_individual_or_family[:coinsurance_rate] = plan_details.search("CoinsuranceRate").first.text.strip
	              plan_details_for_individual_or_family[:family_annual_OOP_limit_amount] = plan_details.search("FamilyAnnualOOPLimitAmount").first.text.strip
	              plan_details_for_individual_or_family[:individual_annual_OOP_limit_amount] = plan_details.search("IndividualAnnualOOPLimitAmount").first.text.strip
	              plan_details_for_individual_or_family[:annual_OOP_elements_text] = plan_details.search("AnnualOOPElementsText").text
	              plan_details_for_individual_or_family[:excluded_annual_OOP_limit_text] = plan_details.search("ExcludedAnnualOOPLimitText").text
	              plan_details_for_individual_or_family[:deductible_exceptions_services_text] = plan_details.search("DeductibleExceptionsServicesText").text

	              plan_details = details_xml.search("IssuerURL")
	              plan_details_for_individual_or_family[:issuer_URL_code] = plan_details.search("URLCode").text
	              plan_details_for_individual_or_family[:issuer_URL_address] = plan_details.search("URLAddress").text

	              plan_details = details_xml.search("ProviderURL")
	              plan_details_for_individual_or_family[:provider_URL_code] = plan_details.search("URLCode").text
	              plan_details_for_individual_or_family[:provider_URL_address] = plan_details.search("URLAddress").text

	              plan_details = details_xml.search("ProductBenefitURL")
	              plan_details_for_individual_or_family[:product_benefit_URL_code] = plan_details.search("URLCode").text
	              plan_details_for_individual_or_family[:product_benefit_URL_address] = plan_details.search("URLAddress").text

	              plan_details = details_xml.search("ProductFormularyURL")
	              plan_details_for_individual_or_family[:product_formulary_URL_code] = plan_details.search("URLCode").text
	              plan_details_for_individual_or_family[:product_formulary_URL_address] = plan_details.search("URLAddress").text

	              plan_details = details_xml.search("PlanBrochureURL")
	              plan_details_for_individual_or_family[:plan_brochure_code] = plan_details.search("URLCode").text
	              plan_details_for_individual_or_family[:plan_brochure_address] = plan_details.search("URLAddress").text


	              plan_details = details_xml.search("OtherPractitionerVisit").search("InNetwork")
	              plan_details_for_individual_or_family[:other_practitioner_visit_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:other_practitioner_visit_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("PrimaryCareVisit").search("InNetwork")
	              plan_details_for_individual_or_family[:primary_care_visit_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:primary_care_visit_in_network_percent] = plan_details.search("Percent").text.to_i
	              plan_details_for_individual_or_family[:primary_care_vist_in_network_limitation_type_text] = ""

	              plan_details = details_xml.search("SpecialistVisit").search("InNetwork")
	              plan_details_for_individual_or_family[:specialist_visit_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:specialist_visit_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("DiagnosticTest").search("InNetwork")
	              plan_details_for_individual_or_family[:diagnostic_test_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:diagnostic_test_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("Imaging").search("InNetwork")
	              plan_details_for_individual_or_family[:imaging_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:imaging_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("OutpatientFacilityFee").search("InNetwork")
	              plan_details_for_individual_or_family[:outpatient_facility_fee_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:outpatient_facility_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("OutpatientPhysicianFee").search("InNetwork")
	              plan_details_for_individual_or_family[:outpatient_physician_fee_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:outpatient_physician_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("EmergencyRoom").search("InNetwork")
	              plan_details_for_individual_or_family[:emergency_room_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:emergency_room_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("UrgentCare").search("InNetwork")
	              plan_details_for_individual_or_family[:urgent_care_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:urgent_care_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("HospitalFacilityFee").search("InNetwork")
	              plan_details_for_individual_or_family[:hospital_facility_fee_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:hospital_facility_fee_in_network_percent] = plan_details.search("Percent").text.to_i


	              plan_details = details_xml.search("HospitalPhysicianFee").search("InNetwork")
	              plan_details_for_individual_or_family[:hospital_physician_fee_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:hospital_physician_fee_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("MentalOutpatient").search("InNetwork")
	              plan_details_for_individual_or_family[:mental_outpatient_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:mental_outpatient_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("PrenatalPostnatalCare").search("InNetwork")
	              plan_details_for_individual_or_family[:prenatal_postnatal_care_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:prenatal_postnatal_care_in_network_percent] = plan_details.search("Percent").text.to_i


	              plan_details = details_xml.search("DeliveryInpatient").search("InNetwork")
	              plan_details_for_individual_or_family[:delivery_inpatient_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:delivery_inpatient_in_network_percent] = plan_details.search("Percent").text.to_i


	              plan_details = details_xml.search("GenericDrugsRetail").search("InNetwork")
	              plan_details_for_individual_or_family[:generic_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:generic_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("PreferredBrandDrugsRetail").search("InNetwork")
	              plan_details_for_individual_or_family[:preferred_brand_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:preferred_brand_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("NonPreferredBrandDrugsRetail").search("InNetwork")
	              plan_details_for_individual_or_family[:non_preferred_brand_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:non_preferred_brand_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details = details_xml.search("SpecialtyDrugsRetail").search("InNetwork")
	              plan_details_for_individual_or_family[:speciality_drugs_retail_in_network_benefit] = plan_details.search("Benefit").text
	              plan_details_for_individual_or_family[:speciality_drugs_retail_in_network_percent] = plan_details.search("Percent").text.to_i

	              plan_details_for_individual_or_family[:acupuncture] = details_xml.search("Acupuncture").text
	              plan_details_for_individual_or_family[:bariatric_surgery] = details_xml.search("BariatricSurgery").text
	              plan_details_for_individual_or_family[:non_emergecy_travel_outside] = details_xml.search("NonEmergencyTravelOutside").text
	              plan_details_for_individual_or_family[:chiropractic] = details_xml.search("Chiropractic").text
	              plan_details_for_individual_or_family[:hearing_aid] = details_xml.search("HearingAid").text
	              plan_details_for_individual_or_family[:infertility_treatment] = details_xml.search("InfertilityTreatment").text
	              plan_details_for_individual_or_family[:long_term_care] = details_xml.search("LongTermCare").text
	              plan_details_for_individual_or_family[:private_nursing] = details_xml.search("PrivateNursing").text
	              plan_details_for_individual_or_family[:eye_exam_adult] = details_xml.search("EyeExamAdult").text
	              plan_details_for_individual_or_family[:routine_foot_care] = details_xml.search("RoutineFootCare").text
	              plan_details_for_individual_or_family[:weight_loss_program] = details_xml.search("WeightLossProgram").text
	              plan_details_for_individual_or_family[:routine_hearing_tests] = details_xml.search("RoutineHearingTests").text
	              plan_details_for_individual_or_family[:specialist_referral_required_services_text] = details_xml.search("SpecialistReferralRequiredServicesText").text

	              plan_details_for_individual_or_family[:maternity_coverage_in_network_benefit] = ""
	              plan_details_for_individual_or_family[:maternity_coverage_in_network_percent] = 0

	              plan_details_for_individual_or_family.save
	            end
	          end

	        end      

	      rescue => ex
	        print "***********************error****************************"
	        print ex.message
	        #print "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	        #logger.debug "***********************error****************************"
	        #logger.debug "ZipCode:" + county_key + "-------Age:" + age_index.to_s
	      end
	    end
		end

	end	
end