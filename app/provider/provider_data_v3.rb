
require 'rest_client'
require 'nokogiri'
require 'open-uri'
require 'csv'


class ProviderDataV3
	
	def log_message(message)
    File.open('debug.log', "a") do |f|
			f << message
		  f.write "\n"
		end
	end

	def extract_counties
		request_uri = "https://api.finder.healthcare.gov/v3.0/getCountiesForZip"
		# for state_key in $USA_zip_codes.keys
		# 	for county_key in $USA_zip_codes[state_key].keys
				
		# 	end
		# end

		line_number = 0;

		CSV.foreach('AllCounty-update_green.csv') do |row|
		  #puts row[0]


			begin

				line_number += 1
				zip_code = row[2]

				if zip_code.length < 5
					for i in 0..5 - zip_code.length - 1
						zip_code = '0' + zip_code
					end
				end
				
				request_data = '<p:ZipCodeValidationRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
				request_data += '<p:ZipCode>'
				request_data += zip_code
				request_data += '</p:ZipCode></p:ZipCodeValidationRequest>'

				print "StateCode:" + row[0] + "--------ZipCode:" + row[1]

				if CountiesForZip.exist_zip_code(zip_code).size == 0

					response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
					xml = Nokogiri::XML(response_data)

	        county_for_zip = CountiesForZip.new
	      	county_for_zip[:zip_code] = zip_code

	      	county_for_zip[:fips_code] = xml.search('FipsCode')[0].text
	      	county_for_zip[:county_name] = xml.search('CountyName')[0].text.upcase
	      	county_for_zip[:state_code] = xml.search('StateCode')[0].text.upcase
	      	if xml.search('FipsCode').size > 1
	      		is_exist = false	
		      	for i in 0..xml.search('FipsCode').size - 1
		      		state_code = xml.search('StateCode')[i].text.upcase
		      		county_name = xml.search('CountyName')[i].text.upcase
		      		if state_code == row[0] and row[1].upcase.include? county_name
		      			county_for_zip[:fips_code] = xml.search('FipsCode')[i].text
				      	county_for_zip[:county_name] = xml.search('CountyName')[i].text.upcase
				      	county_for_zip[:state_code] = xml.search('StateCode')[i].text.upcase
				      	is_exist = true
				      	break
		      		end
		      	end
		      	if !is_exist
		      		message = "MultipleFipsCodeError-------line_number:#{line_number}------StateCode:#{row[0]}-------CountyName:#{row[1]}------ZipCode:#{zip_code}"
	      			log_message message
		      	end
	      	end
	      	

	      	if CountiesForZip.exist_zip_code(zip_code).size == 0
	      		county_for_zip.save
	      	end
      	else
      		#message = "DuplicateZipCodeerror-------line_number:#{line_number}------StateCode:#{row[0]}-------CountyName:#{row[1]}------ZipCode:#{zip_code}"
	      	#log_message message
      	end

			rescue Exception => ex

				print "***********************error****************************"
        print ex.message
        message = "WrongZipCodeError-------line_number:#{line_number}------StateCode:#{row[0]}-------CountyName:#{row[1]}------ZipCode:#{zip_code}"
	      log_message message

			end
		end

	end
	
	def extract_data_v3(state_code,age_from,age_to,gender_type,tobacco_type)
		
		page_size = 100

		full_message = ''

		#for county_for_zip in CountiesForZip.get_zip_code_by_state_code(state_code)
		for county_for_zip in CountiesForZip.all.order(:state_code).reverse_order

			# if county_for_zip[:state_code] !='CA' and county_for_zip[:state_code] !='TX' and county_for_zip[:state_code] !='NC'
			# 	next
			# end

			# if county_for_zip[:state_code] !='MO' and county_for_zip[:state_code] !='KY'
			# 	next
			# end

			if county_for_zip[:state_code] != state_code and state_code.upcase != 'ALL'
				next
			end
			# compare_string = county_for_zip[:state_code] <=> "OH"

			# if compare_string == -1
			# 	next
			# end

			for age_index in age_from..age_to

				page_number = 1

				while true
					current_date = age_index.years.ago.beginning_of_year
					current_date = Date.new(1995, 4, 2)

		      request_uri = "https://api.finder.healthcare.gov/v3.0/getIFPPlanQuotes"

		      request_data = '<?xml version="1.0" encoding="UTF-8"?>'
		      request_data += '<p:PlanQuoteRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
		      request_data += '<p:Enrollees><p1:DateOfBirth>'
		      request_data += current_date.strftime("%Y-%m-%d")
		      request_data += '</p1:DateOfBirth><p1:Gender>'
		      request_data += gender_type
		      request_data += '</p1:Gender><p1:TobaccoLastUsedMonths>2'
		      request_data += '</p1:TobaccoLastUsedMonths><p1:Relation>SELF</p1:Relation><p1:InHouseholdIndicator>true</p1:InHouseholdIndicator></p:Enrollees>'
		      request_data += '<p:Location><p1:ZipCode>'
		      request_data += county_for_zip[:zip_code]
		      request_data += '</p1:ZipCode><p1:County>'
		      request_data += '<p1:FipsCode>'
		      request_data += county_for_zip[:fips_code]
		      request_data += '</p1:FipsCode>'
		      request_data += '<p1:CountyName>'
		      request_data += county_for_zip[:county_name]
		      request_data += '</p1:CountyName><p1:StateCode>'
		      request_data += county_for_zip[:state_code]
		      request_data += '</p1:StateCode></p1:County></p:Location>'
		      request_data += '<p:InsuranceEffectiveDate>2015-01-01</p:InsuranceEffectiveDate>'
		      request_data += '<p:Market>Individual</p:Market><p:IsFilterAnalysisRequiredIndicator>false</p:IsFilterAnalysisRequiredIndicator>'
		      request_data += '<p:PaginationInformation><p1:PageNumber>'
		      request_data += page_number.to_s
		      request_data += '</p1:PageNumber><p1:PageSize>'
		      request_data += page_size.to_s
		      request_data += '</p1:PageSize></p:PaginationInformation>'
		      request_data += '<p:SortOrder><p1:SortField>BASE RATE</p1:SortField><p1:SortDirection>ASC</p1:SortDirection></p:SortOrder></p:PlanQuoteRequest>'
		      
		      
		      begin
		        print "#{county_for_zip[:state_code]}-----#{county_for_zip[:county_name]}-----#{age_index.to_s}"
		        #logger.debug "ZipCode:" + county_key + "-------Age:" + age_index.to_s
		        
		        response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
		        #log_message request_data
		        #response_data = $test_response_data_quote
		        
		        xml = Nokogiri::XML(response_data)
		        plan_ids = xml.search("PlanID")

		        total_plans_count = plan_ids.size
		        if total_plans_count == 0
		        	#print '--------------no got it--------------'
		        	break
		        else
		        	#print '--------------got it--------------'
		        	#return
		        end
        
		        plan_name_texts = xml.search("PlanNameText")
		        product_name_texts = xml.search("ProductNameText")
		        issuer_ids = xml.search("IssuerID")
		        issuer_name_texts = xml.search("IssuerNameText")
		        issuer_state_codes = xml.search("IssuerStateCode")
		        provider_types = xml.search("ProviderType")
		        base_rate_amounts = xml.search("BaseRateAmount")
		        metal_tier_types = xml.search("MetalTierType")
		        child_only_indicators = xml.search("ChildOnlyIndicator")
		        hsa_eligible_indicators = xml.search("HSAEligibleIndicator")
		        same_sex_partners = xml.search("SameSexPartnerIndicator")
		        domestic_partners = xml.search("DomesticPartnerIndicator")
		        maternity_coverage_indicators = xml.search("MaternityCoverageIndicator")
		        mental_coverage_indicators = xml.search("MentalCoverageIndicator")
		        #prescription_in_network_coverage_indicators = xml.search("PlanID")
		        substance_abuse_coverage_indicators = xml.search("SubstanceAbuseCoverageIndicator")
		        drug_coverage_indicators = xml.search("DrugCoverageIndicator")
		        dental_coverage_indicators = xml.search("DentalCoverageIndicator")
		        vision_coverage_indicators = xml.search("VisionCoverageIndicator")
		        wellness_program_coverage_indicators = xml.search("WellnessPorgramCoverageIndicator")

		        for i in 0..total_plans_count - 1

		          ifp_plan_quote = IfpPlanQuote.new
	          
		          ifp_plan_quote[:plan_id] = plan_ids[i].text
		          ifp_plan_quote[:plan_name_text] = plan_name_texts[i].text
		          ifp_plan_quote[:product_name_text] = product_name_texts[i].text
		          ifp_plan_quote[:issuer_id] = issuer_ids[i].text
		          ifp_plan_quote[:issuer_name_text] = issuer_name_texts[i].text
		          ifp_plan_quote[:issuer_state_code] = issuer_state_codes[i].text
		          ifp_plan_quote[:provider_type] = provider_types[i].text
		          ifp_plan_quote[:base_rate_amount] = base_rate_amounts[i].text
		          ifp_plan_quote[:metal_tier_type] = metal_tier_types[i].text
		          ifp_plan_quote[:child_only_indicator] = child_only_indicators[i].text
		          ifp_plan_quote[:hsa_eligible_indicator] = hsa_eligible_indicators[i].text
		          ifp_plan_quote[:same_sex_partner] = same_sex_partners[i].text
		          ifp_plan_quote[:domestic_partner] = domestic_partners[i].text
		          ifp_plan_quote[:maternity_coverage_indicator] = maternity_coverage_indicators[i].text
		          ifp_plan_quote[:mental_coverage_indicator] = mental_coverage_indicators[i].text
		          ifp_plan_quote[:prescription_in_network_coverage_indicator] = ''
		          ifp_plan_quote[:substance_abuse_coverage_indicator] = substance_abuse_coverage_indicators[i].text
		          ifp_plan_quote[:drug_coverage_indicator] = drug_coverage_indicators[i].text
		          ifp_plan_quote[:dental_coverage_indicator] = dental_coverage_indicators[i].text
		          ifp_plan_quote[:vision_coverage_indicator] = vision_coverage_indicators[i].text
		          ifp_plan_quote[:wellness_program_coverage_indicator] = wellness_program_coverage_indicators[i].text
		          ifp_plan_quote[:age] = age_index.to_s
		          ifp_plan_quote[:effective_date] = '2015-01-01'
		          ifp_plan_quote[:zipcode] = county_for_zip[:zip_code]
		          ifp_plan_quote[:gender_type] = gender_type
		          ifp_plan_quote[:tobacco_user] = ''

		          if IfpPlanQuote.exist_plan_id(plan_ids[i].text).size != 0
		          	next
		          else
		          	message = "#{county_for_zip[:state_code]}-----#{county_for_zip[:county_name]}-----#{age_index.to_s}-------#{plan_ids[i].text}"
		          	log_message message

		          	ifp_plan_quote.save

		          	request_uri = "https://api.finder.healthcare.gov/v3.0/getIFPPlanBenefits"

		          	request_data = '<?xml version="1.0" encoding="UTF-8"?>'
		          	request_data += '<p:PlanBenefitRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" 
		          		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
		          	request_data += '<p:Enrollees><p1:DateOfBirth>'
		          	request_data += current_date.strftime("%Y-%m-%d")
		          	request_data += '</p1:DateOfBirth><p1:Gender>'
		          	request_data += gender_type
		          	request_data += '</p1:Gender><p1:TobaccoLastUsedMonths>2</p1:TobaccoLastUsedMonths><p1:Relation>SELF</p1:Relation>'
		          	request_data += '<p1:InHouseholdIndicator>true</p1:InHouseholdIndicator></p:Enrollees>'
		          	request_data += '<p:Location><p1:ZipCode>'
		          	request_data += county_for_zip[:zip_code]
		          	request_data += '</p1:ZipCode><p1:County><p1:FipsCode>'
		          	request_data += county_for_zip[:fips_code]
		          	request_data += '</p1:FipsCode><p1:CountyName>'
		          	request_data += county_for_zip[:county_name]
		          	request_data += '</p1:CountyName><p1:StateCode>'
		          	request_data += county_for_zip[:state_code]
		          	request_data += '</p1:StateCode></p1:County></p:Location>'
		          	request_data += '<p:InsuranceEffectiveDate>2015-01-01</p:InsuranceEffectiveDate><p:Market>Individual</p:Market>'
		          	request_data += '<p:PlanIds><p:PlanId>'
		          	request_data += plan_ids[i].text
		          	request_data += '</p:PlanId></p:PlanIds></p:PlanBenefitRequest>'

		          	full_message = request_data

		            details_response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
		            #details_response_data = $test_response_data_benefit

		            ifp_plan_benefit = IfpPlanBenefit.new
		            details_xml = Nokogiri::XML(details_response_data)

		            if details_xml.search("ReturnCode").text == "Error"
		            	message = "There is no data available for this planid."
		            	log_message message
		            	next
		            end

		            plan_details = details_xml.search("FamilyAnnualDeductibleAmount")

		            ifp_plan_benefit[:plan_id] = plan_ids[i].text
		            ifp_plan_benefit[:family_deductible_amount] = details_xml.search("FamilyAnnualDeductibleAmount").first.text.strip
		            ifp_plan_benefit[:individual_deductile_amount] = details_xml.search("IndividualAnnualDeductibleAmount").first.text.strip
		            ifp_plan_benefit[:family_annual_OOP_limit_amount] = details_xml.search("FamilyAnnualOOPLimitAmount").first.text.strip
		            ifp_plan_benefit[:individual_annual_OOP_limit_amount] = details_xml.search("IndividualAnnualOOPLimitAmount").first.text.strip
		            
		            plan_details = details_xml.search("IssuerURL")
		            ifp_plan_benefit[:issuer_URL_code] = plan_details.search("URLCode").text
		            ifp_plan_benefit[:issuer_URL_address] = plan_details.search("URLAddress").text

		            plan_details = details_xml.search("ProviderURL")
		            ifp_plan_benefit[:provider_URL_code] = plan_details.search("URLCode").text
		            ifp_plan_benefit[:provider_URL_address] = plan_details.search("URLAddress").text

		            plan_details = details_xml.search("ProductBenefitURL")
		            ifp_plan_benefit[:product_benefit_URL_code] = plan_details.search("URLCode").text
		            ifp_plan_benefit[:product_benefit_URL_address] = plan_details.search("URLAddress").text

		            plan_details = details_xml.search("PlanSummaryBenefitURL")
		            ifp_plan_benefit[:plan_summary_URL_code] = plan_details.search("URLCode").text
		            ifp_plan_benefit[:plan_summary_URL_address] = plan_details.search("URLAddress").text

		            plan_details = details_xml.search("PlanBrochureURL")
		            ifp_plan_benefit[:plan_brochure_URL_code] = plan_details.search("URLCode").text
		            ifp_plan_benefit[:plan_brochure_URL_address] = plan_details.search("URLAddress").text

		            ifp_plan_benefit[:disease_management_programs_offered_text] = details_xml.search("DiseaseManagementProgramsOfferedText").text


		            
		            plan_details = details_xml.search("PrimaryCareVisit").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:primary_care_visit_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:primary_care_visit_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("PrimaryCareVisit").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:primary_care_visit_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:primary_care_visit_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("SpecialistVisit").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:specialist_visit_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:specialist_visit_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("SpecialistVisit").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:specialist_visit_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:specialist_visit_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            ifp_plan_benefit[:referral_required_specialist_indicator] = details_xml.search("ReferralRequiredSpecialistIndicator").text

		            plan_details = details_xml.search("Imaging").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:imaging_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:imaging_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("Imaging").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:imaging_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            
		            plan_details = details_xml.search("XRayDiagnosticImaging").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:xray_diagnostic_imaging_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            plan_details = details_xml.search("XRayDiagnosticImaging").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:xray_diagnostic_imaging_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:xray_diagnostic_imaging_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            
		            plan_details = details_xml.search("LaboratoryOutPatientProfessionalServices").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:laboratory_outpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            plan_details = details_xml.search("LaboratoryOutPatientProfessionalServices").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:laboratory_outpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text

		            plan_details = details_xml.search("InpatientHospitalServices").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:inpatient_hospital_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:inpatient_hospital_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("InpatientHospitalServices").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:inpatient_hospital_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:inpatient_hospital_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text.to_i

		            plan_details = details_xml.search("InpatientPhysicianAndSurgicalService").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:inpatient_physician_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:inpatient_physician_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("InpatientPhysicianAndSurgicalService").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:inpatient_physician_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text

		            ifp_plan_benefit[:maximum_numberofdays_charging_inpatient_copay] = details_xml.search("MaximumNumberOfDaysChargingInpatientCopay").text

		            plan_details = details_xml.search("EmergencyRoomServices").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:emergency_room_services_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            plan_details = details_xml.search("EmergencyRoomServices").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:emergency_room_services_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:emergency_room_services_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text
					            

		            plan_details = details_xml.search("EmergencyTransportationAmbulanceService").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:emergency_transportation_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:emergency_transportation_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("EmergencyTransportationAmbulanceService").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:emergency_transportation_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:emergency_transportation_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("GenericDrugs").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:generic_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:generic_drugs_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("GenericDrugs").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:generic_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:generic_drugs_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("PreferredBrandDrugs").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:preferred_brand_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:preferred_brand_drugs_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("PreferredBrandDrugs").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:preferred_brand_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:preferred_brand_drugs_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("NonPreferredBrandDrugs").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:non_preferred_brand_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:non_preferred_brand_drugs_co_pay_in_network_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("NonPreferredBrandDrugs").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:non_preferred_brand_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            
		            plan_details = details_xml.search("SpecialityDrugs").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:speciality_drugs_co_pay_in_network_benefit] = plan_details.search("Benefit").text
		            plan_details = details_xml.search("SpecialityDrugs").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:speciality_drugs_co_insurance_in_network_benefit] = plan_details.search("Benefit").text
		            
		            plan_details = details_xml.search("MentalHealthOutPatientServices").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:mental_health_outpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:mental_health_outpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("MentalHealthOutPatientServices").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("MentalHealthIntPatientServices").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:mental_health_inpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:mental_health_inpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("MentalHealthIntPatientServices").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:mental_health_inpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            ifp_plan_benefit[:notice_required_for_pregnancy_indicator] = details_xml.search("NoticeRequiredForPregnancyIndicator").text

		            plan_details = details_xml.search("PrenatalAndPostnatalCare").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:prenatal_postnatal_care_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            plan_details = details_xml.search("PrenatalAndPostnatalCare").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:prenatal_postnatal_care_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:prenatal_postnatal_care_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("DeliveryAndAllInpatientServicesMaternityCare").search("CoPayInNetworkTier1")
		            ifp_plan_benefit[:delivery_all_inpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:delivery_all_inpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
		            plan_details = details_xml.search("DeliveryAndAllInpatientServicesMaternityCare").search("CoInsuranceInNetworkTier1")
		            ifp_plan_benefit[:delivery_all_inpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
		            ifp_plan_benefit[:delivery_all_inpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

		            plan_details = details_xml.search("IncludedBenefits")
		            ifp_plan_benefit[:included_benefit_name] = details_xml.search("IncludedBenefits").text
		            ifp_plan_benefit[:excluded_benefit_name] = details_xml.search("ExcludedBenefits").text
		            ifp_plan_benefit[:limited_benefit_name] = details_xml.search("LimitedBenefits").text

		            

		            ifp_plan_benefit.save

		          end
		        end
		        
		        if total_plans_count < page_size
		        	break
		        else
		        	page_number += 1
		        end

		      rescue => ex
		        print "***********************error****************************"
		        print ex.message
		        message = "**************error************ZipCode:" + county_key + "-------Age:" + age_index.to_s
						log_message message
		        break
		      end
				end
	    end
		end

	end	


	def revision
		for plan_quote in IfpPlanQuote.all

			begin
				

				if IfpPlanBenefit.get_plan_benefit_by_plan_id(plan_quote[:plan_id]).size == 0

					request_uri = "https://api.finder.healthcare.gov/v3.0/getIFPPlanBenefits"

	      	request_data = '<?xml version="1.0" encoding="UTF-8"?>'
	      	request_data += '<p:PlanBenefitRequest xmlns:p="http://hios.cms.org/api" xmlns:p1="http://hios.cms.org/api-types" 
	      		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hios.cms.org/api hios-api-11.0.xsd ">'
	      	request_data += '<p:Enrollees><p1:DateOfBirth>'
	      	request_data += current_date.strftime("%Y-%m-%d")
	      	request_data += '</p1:DateOfBirth><p1:Gender>'
	      	request_data += gender_type
	      	request_data += '</p1:Gender><p1:TobaccoLastUsedMonths>2</p1:TobaccoLastUsedMonths><p1:Relation>SELF</p1:Relation>'
	      	request_data += '<p1:InHouseholdIndicator>true</p1:InHouseholdIndicator></p:Enrollees>'
	      	request_data += '<p:Location><p1:ZipCode>'
	      	request_data += county_for_zip[:zip_code]
	      	request_data += '</p1:ZipCode><p1:County><p1:FipsCode>'
	      	request_data += county_for_zip[:fips_code]
	      	request_data += '</p1:FipsCode><p1:CountyName>'
	      	request_data += county_for_zip[:county_name]
	      	request_data += '</p1:CountyName><p1:StateCode>'
	      	request_data += county_for_zip[:state_code]
	      	request_data += '</p1:StateCode></p1:County></p:Location>'
	      	request_data += '<p:InsuranceEffectiveDate>2015-01-01</p:InsuranceEffectiveDate><p:Market>Individual</p:Market>'
	      	request_data += '<p:PlanIds><p:PlanId>'
	      	request_data += plan_ids[i].text
	      	request_data += '</p:PlanId></p:PlanIds></p:PlanBenefitRequest>'

	      	
	        details_response_data = RestClient.post(request_uri, request_data, :content_type=>'application/xml')
	        
	        ifp_plan_benefit = IfpPlanBenefit.new
	        details_xml = Nokogiri::XML(details_response_data)

	        if details_xml.search("ReturnCode").text == "Error"
	        	message = "There is no data available for this planid."
	        	log_message message
	        	next
	        end

	        plan_details = details_xml.search("FamilyAnnualDeductibleAmount")

	        ifp_plan_benefit[:plan_id] = plan_ids[i].text
	        ifp_plan_benefit[:family_deductible_amount] = details_xml.search("FamilyAnnualDeductibleAmount").first.text.strip
	        ifp_plan_benefit[:individual_deductile_amount] = details_xml.search("IndividualAnnualDeductibleAmount").first.text.strip
	        ifp_plan_benefit[:family_annual_OOP_limit_amount] = details_xml.search("FamilyAnnualOOPLimitAmount").first.text.strip
	        ifp_plan_benefit[:individual_annual_OOP_limit_amount] = details_xml.search("IndividualAnnualOOPLimitAmount").first.text.strip
	        
	        plan_details = details_xml.search("IssuerURL")
	        ifp_plan_benefit[:issuer_URL_code] = plan_details.search("URLCode").text
	        ifp_plan_benefit[:issuer_URL_address] = plan_details.search("URLAddress").text

	        plan_details = details_xml.search("ProviderURL")
	        ifp_plan_benefit[:provider_URL_code] = plan_details.search("URLCode").text
	        ifp_plan_benefit[:provider_URL_address] = plan_details.search("URLAddress").text

	        plan_details = details_xml.search("ProductBenefitURL")
	        ifp_plan_benefit[:product_benefit_URL_code] = plan_details.search("URLCode").text
	        ifp_plan_benefit[:product_benefit_URL_address] = plan_details.search("URLAddress").text

	        plan_details = details_xml.search("PlanSummaryBenefitURL")
	        ifp_plan_benefit[:plan_summary_URL_code] = plan_details.search("URLCode").text
	        ifp_plan_benefit[:plan_summary_URL_address] = plan_details.search("URLAddress").text

	        plan_details = details_xml.search("PlanBrochureURL")
	        ifp_plan_benefit[:plan_brochure_URL_code] = plan_details.search("URLCode").text
	        ifp_plan_benefit[:plan_brochure_URL_address] = plan_details.search("URLAddress").text

	        ifp_plan_benefit[:disease_management_programs_offered_text] = details_xml.search("DiseaseManagementProgramsOfferedText").text


	        
	        plan_details = details_xml.search("PrimaryCareVisit").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:primary_care_visit_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:primary_care_visit_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("PrimaryCareVisit").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:primary_care_visit_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:primary_care_visit_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("SpecialistVisit").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:specialist_visit_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:specialist_visit_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("SpecialistVisit").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:specialist_visit_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:specialist_visit_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        ifp_plan_benefit[:referral_required_specialist_indicator] = details_xml.search("ReferralRequiredSpecialistIndicator").text

	        plan_details = details_xml.search("Imaging").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:imaging_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:imaging_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("Imaging").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:imaging_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        
	        plan_details = details_xml.search("XRayDiagnosticImaging").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:xray_diagnostic_imaging_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        plan_details = details_xml.search("XRayDiagnosticImaging").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:xray_diagnostic_imaging_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:xray_diagnostic_imaging_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        
	        plan_details = details_xml.search("LaboratoryOutPatientProfessionalServices").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:laboratory_outpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        plan_details = details_xml.search("LaboratoryOutPatientProfessionalServices").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:laboratory_outpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text

	        plan_details = details_xml.search("InpatientHospitalServices").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:inpatient_hospital_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:inpatient_hospital_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("InpatientHospitalServices").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:inpatient_hospital_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:inpatient_hospital_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text.to_i

	        plan_details = details_xml.search("InpatientPhysicianAndSurgicalService").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:inpatient_physician_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:inpatient_physician_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("InpatientPhysicianAndSurgicalService").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:inpatient_physician_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text

	        ifp_plan_benefit[:maximum_numberofdays_charging_inpatient_copay] = details_xml.search("MaximumNumberOfDaysChargingInpatientCopay").text

	        plan_details = details_xml.search("EmergencyRoomServices").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:emergency_room_services_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        plan_details = details_xml.search("EmergencyRoomServices").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:emergency_room_services_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:emergency_room_services_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text
		            

	        plan_details = details_xml.search("EmergencyTransportationAmbulanceService").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:emergency_transportation_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:emergency_transportation_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("EmergencyTransportationAmbulanceService").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:emergency_transportation_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:emergency_transportation_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("GenericDrugs").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:generic_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:generic_drugs_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("GenericDrugs").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:generic_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:generic_drugs_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("PreferredBrandDrugs").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:preferred_brand_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:preferred_brand_drugs_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("PreferredBrandDrugs").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:preferred_brand_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:preferred_brand_drugs_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("NonPreferredBrandDrugs").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:non_preferred_brand_drugs_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:non_preferred_brand_drugs_co_pay_in_network_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("NonPreferredBrandDrugs").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:non_preferred_brand_drugs_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        
	        plan_details = details_xml.search("SpecialityDrugs").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:speciality_drugs_co_pay_in_network_benefit] = plan_details.search("Benefit").text
	        plan_details = details_xml.search("SpecialityDrugs").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:speciality_drugs_co_insurance_in_network_benefit] = plan_details.search("Benefit").text
	        
	        plan_details = details_xml.search("MentalHealthOutPatientServices").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:mental_health_outpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:mental_health_outpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("MentalHealthOutPatientServices").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("MentalHealthIntPatientServices").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:mental_health_inpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:mental_health_inpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("MentalHealthIntPatientServices").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:mental_health_inpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:mental_health_outpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        ifp_plan_benefit[:notice_required_for_pregnancy_indicator] = details_xml.search("NoticeRequiredForPregnancyIndicator").text

	        plan_details = details_xml.search("PrenatalAndPostnatalCare").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:prenatal_postnatal_care_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        plan_details = details_xml.search("PrenatalAndPostnatalCare").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:prenatal_postnatal_care_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:prenatal_postnatal_care_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("DeliveryAndAllInpatientServicesMaternityCare").search("CoPayInNetworkTier1")
	        ifp_plan_benefit[:delivery_all_inpatient_co_pay_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:delivery_all_inpatient_co_pay_in_network_tier1_amount] = plan_details.search("Amount").text
	        plan_details = details_xml.search("DeliveryAndAllInpatientServicesMaternityCare").search("CoInsuranceInNetworkTier1")
	        ifp_plan_benefit[:delivery_all_inpatient_co_insurance_in_network_tier1_benefit] = plan_details.search("Benefit").text
	        ifp_plan_benefit[:delivery_all_inpatient_co_insurance_in_network_tier1_percent] = plan_details.search("Percent").text

	        plan_details = details_xml.search("IncludedBenefits")
	        ifp_plan_benefit[:included_benefit_name] = details_xml.search("IncludedBenefits").text
	        ifp_plan_benefit[:excluded_benefit_name] = details_xml.search("ExcludedBenefits").text
	        ifp_plan_benefit[:limited_benefit_name] = details_xml.search("LimitedBenefits").text

	        

	        ifp_plan_benefit.save
				end


			rescue Exception => e
				message = "**************error************PlanID:" + plan_quote[:plan_id]
				log_message message
			end

			

		end
	end
end