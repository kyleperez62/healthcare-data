// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

function returnToPreviousPage() {
    window.history.back();
}

function validate_filters() {
	if ($('#county_zip_code').val() == '') {
		alert('Please enter the County ZipCode');
		returnToPreviousPage();
		return false;
	}
	if ($('#age_from').val() == '') {
		alert('Please enter the from-age');
		return;
	}
	if ($('#age_to').val() == '') {
		alert('Please enter the to-age');
		return;
	}
	if (isNaN($('#age_from').val())) {
		alert('please enter the correct from-age');
		return;
	}
	if (isNaN($('#age_to').val())) {
		alert('please enter the correct to-age');
		return;
	}
	if (parseInt($('#age_from').val()) > parseInt($('#age_to').val())) {
		alert('from-age cannot be larger than to-age.');
		return;
	}

	$('.state_message').css('display', 'block');
	$('.state_message').html('pulling...')

	request_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <PrivateOptionsAPIRequest><CountiesForPostalCodeRequest><ZipCode>20110</ZipCode></CountiesForPostalCodeRequest></PrivateOptionsAPIRequest>";

	//alert(request_data);

	// $.ajax({
 //    type: "POST",
 //    url: "https://api.finder.healthcare.gov/v2.0/",
 //    data: request_data,
 //    success: function(data) {
 //      //$('#state_message').html(data);
 //      alert(data);
      
 //    }

 //  });

 //  alert('end');

}
