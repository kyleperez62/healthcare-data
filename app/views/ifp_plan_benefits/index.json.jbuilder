json.array!(@ifp_plan_benefits) do |ifp_plan_benefit|
  json.extract! ifp_plan_benefit, :id
  json.url ifp_plan_benefit_url(ifp_plan_benefit, format: :json)
end
