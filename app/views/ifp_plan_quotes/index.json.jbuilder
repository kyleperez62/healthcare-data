json.array!(@ifp_plan_quotes) do |ifp_plan_quote|
  json.extract! ifp_plan_quote, :id
  json.url ifp_plan_quote_url(ifp_plan_quote, format: :json)
end
