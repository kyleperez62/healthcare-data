json.array!(@plans_for_individual_or_families) do |plans_for_individual_or_family|
  json.extract! plans_for_individual_or_family, :id
  json.url plans_for_individual_or_family_url(plans_for_individual_or_family, format: :json)
end
