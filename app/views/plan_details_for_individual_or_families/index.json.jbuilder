json.array!(@plan_details_for_individual_or_families) do |plan_details_for_individual_or_family|
  json.extract! plan_details_for_individual_or_family, :id
  json.url plan_details_for_individual_or_family_url(plan_details_for_individual_or_family, format: :json)
end
