class CountiesForZip < ActiveRecord::Base
	scope :exist_zip_code, ->(zip_code) { where(:zip_code => zip_code) }
	scope :get_zip_code_by_state_code, ->(state_code) { where(:state_code => state_code)}
end
