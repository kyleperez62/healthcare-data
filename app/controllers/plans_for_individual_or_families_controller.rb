
require 'rest_client'
require 'nokogiri'
require 'open-uri'






class PlansForIndividualOrFamiliesController < ApplicationController
  before_action :set_plans_for_individual_or_family, only: [:show, :edit, :update, :destroy]

  # GET /plans_for_individual_or_families
  # GET /plans_for_individual_or_families.json
  def index
    #@plans_for_individual_or_families = PlansForIndividualOrFamily.all.limit(10)
    @plans_for_individual_or_families = PlansForIndividualOrFamily.all.limit(100)
    
  end

  def pull_data
    @plans_for_individual_or_families = PlansForIndividualOrFamily.all
  end

  

  def pull_datas

    request_uri = "https://api.finder.healthcare.gov/v2.0/"


    age_from = params[:age_from].to_i
    age_to = params[:age_to].to_i
    gender_type = params[:sex]
    tobacco_type = params[:tobacco_type]
    state_code = params[:USA_state_code]

    plan_data = ProviderData.new
    plan_data.extract_data state_code,age_from,age_to,gender_type,tobacco_type 
    redirect_to(:controller => 'plans_for_individual_or_families', :action => 'index')
    
  end

  # GET /plans_for_individual_or_families/1
  # GET /plans_for_individual_or_families/1.json
  def show
  end

  # GET /plans_for_individual_or_families/new
  def new
    @plans_for_individual_or_family = PlansForIndividualOrFamily.new
  end

  # GET /plans_for_individual_or_families/1/edit
  def edit
  end

  # POST /plans_for_individual_or_families
  # POST /plans_for_individual_or_families.json
  def create
    @plans_for_individual_or_family = PlansForIndividualOrFamily.new(plans_for_individual_or_family_params)

    respond_to do |format|
      if @plans_for_individual_or_family.save
        format.html { redirect_to @plans_for_individual_or_family, notice: 'Plans for individual or family was successfully created.' }
        format.json { render :show, status: :created, location: @plans_for_individual_or_family }
      else
        format.html { render :new }
        format.json { render json: @plans_for_individual_or_family.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plans_for_individual_or_families/1
  # PATCH/PUT /plans_for_individual_or_families/1.json
  def update
    respond_to do |format|
      if @plans_for_individual_or_family.update(plans_for_individual_or_family_params)
        format.html { redirect_to @plans_for_individual_or_family, notice: 'Plans for individual or family was successfully updated.' }
        format.json { render :show, status: :ok, location: @plans_for_individual_or_family }
      else
        format.html { render :edit }
        format.json { render json: @plans_for_individual_or_family.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plans_for_individual_or_families/1
  # DELETE /plans_for_individual_or_families/1.json
  def destroy
    @plans_for_individual_or_family.destroy
    respond_to do |format|
      format.html { redirect_to plans_for_individual_or_families_url, notice: 'Plans for individual or family was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plans_for_individual_or_family
      @plans_for_individual_or_family = PlansForIndividualOrFamily.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plans_for_individual_or_family_params
      params[:plans_for_individual_or_family]
    end
end
