
require 'rest_client'
require 'nokogiri'
require 'open-uri'

$zip_codes = {
  "Alameda" => "94501", "Alpine" => "96120", "Amador" => "95629", "Calaveras" => "95222", "Colusa" => "95912", "Contra Costa" => "94505", 
  "Del Norte" => "95531", "El Dorado" => "95614", "Fresno" => "93210", "Glenn" =>"95920", "Imperial" => "92227", "Inyo" => "92328", 
  "Kern" => "93203", "Lassen" => "96056", "Los Angeles" => "90001", "Madera" => "93601", "Marin" => "94901", "Mariposa" => "93623", 
  "Mendocino" => "95410", "Merced" => "93620", "Modoc" => "96006", "Mono" => "93512", "Monterey" => "93426", "Napa" => "94503", 
  "Placer" => "95602", "Plumas" => "95923", "Riverside" => "91752", "Sacramento" => "95608", "San Benito" => "95023", "San Bernardino" => "91701", 
  "San Diego" => "91901", "San Francisco" => "94101", "San Joaquin" => "95202", "San Luis Obispo" => "93401", "San Mateo" => "94002", 
  "Santa Barbara" => "93013", "Santa Clara" => "94022", "Shasta" => "96001", "Siskiyou" => "95568", "Solano" => "94510", 
  "Sonoma" => "94922", "Stanislaus" => "95230", "Sutter" => "95645", "Tehama" => "96021", "Tulare" => "93207", "Tuolumne" => "95310", 
  "Ventura" => "91320", "Yolo" => "95605", "Yuba" => "95692"
}

class PlanDetailsForIndividualOrFamiliesController < ApplicationController
  before_action :set_plan_details_for_individual_or_family, only: [:show, :edit, :update, :destroy]

  # GET /plan_details_for_individual_or_families
  # GET /plan_details_for_individual_or_families.json
  def index
    @plan_details_for_individual_or_families = PlanDetailsForIndividualOrFamily.all
  end

  # GET /plan_details_for_individual_or_families/1
  # GET /plan_details_for_individual_or_families/1.json
  def show
  end

  # GET /plan_details_for_individual_or_families/new
  def new
    @plan_details_for_individual_or_family = PlanDetailsForIndividualOrFamily.new
  end

  # GET /plan_details_for_individual_or_families/1/edit
  def edit
  end

  # POST /plan_details_for_individual_or_families
  # POST /plan_details_for_individual_or_families.json
  def create
    @plan_details_for_individual_or_family = PlanDetailsForIndividualOrFamily.new(plan_details_for_individual_or_family_params)

    respond_to do |format|
      if @plan_details_for_individual_or_family.save
        format.html { redirect_to @plan_details_for_individual_or_family, notice: 'Plan details for individual or family was successfully created.' }
        format.json { render :show, status: :created, location: @plan_details_for_individual_or_family }
      else
        format.html { render :new }
        format.json { render json: @plan_details_for_individual_or_family.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plan_details_for_individual_or_families/1
  # PATCH/PUT /plan_details_for_individual_or_families/1.json
  def update
    respond_to do |format|
      if @plan_details_for_individual_or_family.update(plan_details_for_individual_or_family_params)
        format.html { redirect_to @plan_details_for_individual_or_family, notice: 'Plan details for individual or family was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_details_for_individual_or_family }
      else
        format.html { render :edit }
        format.json { render json: @plan_details_for_individual_or_family.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_details_for_individual_or_families/1
  # DELETE /plan_details_for_individual_or_families/1.json
  def destroy
    @plan_details_for_individual_or_family.destroy
    respond_to do |format|
      format.html { redirect_to plan_details_for_individual_or_families_url, notice: 'Plan details for individual or family was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_details_for_individual_or_family
      @plan_details_for_individual_or_family = PlanDetailsForIndividualOrFamily.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_details_for_individual_or_family_params
      params[:plan_details_for_individual_or_family]
    end
end
