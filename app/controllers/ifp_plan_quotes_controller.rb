class IfpPlanQuotesController < ApplicationController
  before_action :set_ifp_plan_quote, only: [:show, :edit, :update, :destroy]

  # GET /ifp_plan_quotes
  # GET /ifp_plan_quotes.json
  def index
    @ifp_plan_quotes = IfpPlanQuote.all
  end

  # GET /ifp_plan_quotes/1
  # GET /ifp_plan_quotes/1.json
  def show
  end

  # GET /ifp_plan_quotes/new
  def new
    @ifp_plan_quote = IfpPlanQuote.new
  end

  def pull_datas
    plan = ProviderDataV3.new
    plan.extract_counties
    redirect_to(:controller => 'ifp_plan_quotes', :action => 'index')
  end
  # GET /ifp_plan_quotes/1/edit
  def edit
  end

  # POST /ifp_plan_quotes
  # POST /ifp_plan_quotes.json
  def create
    @ifp_plan_quote = IfpPlanQuote.new(ifp_plan_quote_params)

    respond_to do |format|
      if @ifp_plan_quote.save
        format.html { redirect_to @ifp_plan_quote, notice: 'Ifp plan quote was successfully created.' }
        format.json { render :show, status: :created, location: @ifp_plan_quote }
      else
        format.html { render :new }
        format.json { render json: @ifp_plan_quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ifp_plan_quotes/1
  # PATCH/PUT /ifp_plan_quotes/1.json
  def update
    respond_to do |format|
      if @ifp_plan_quote.update(ifp_plan_quote_params)
        format.html { redirect_to @ifp_plan_quote, notice: 'Ifp plan quote was successfully updated.' }
        format.json { render :show, status: :ok, location: @ifp_plan_quote }
      else
        format.html { render :edit }
        format.json { render json: @ifp_plan_quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ifp_plan_quotes/1
  # DELETE /ifp_plan_quotes/1.json
  def destroy
    @ifp_plan_quote.destroy
    respond_to do |format|
      format.html { redirect_to ifp_plan_quotes_url, notice: 'Ifp plan quote was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ifp_plan_quote
      @ifp_plan_quote = IfpPlanQuote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ifp_plan_quote_params
      params[:ifp_plan_quote]
    end
end
