class IfpPlanBenefitsController < ApplicationController
  before_action :set_ifp_plan_benefit, only: [:show, :edit, :update, :destroy]

  # GET /ifp_plan_benefits
  # GET /ifp_plan_benefits.json
  def index
    @ifp_plan_benefits = IfpPlanBenefit.all
  end

  # GET /ifp_plan_benefits/1
  # GET /ifp_plan_benefits/1.json
  def show
  end

  # GET /ifp_plan_benefits/new
  def new
    @ifp_plan_benefit = IfpPlanBenefit.new
  end

  # GET /ifp_plan_benefits/1/edit
  def edit
  end

  # POST /ifp_plan_benefits
  # POST /ifp_plan_benefits.json
  def create
    @ifp_plan_benefit = IfpPlanBenefit.new(ifp_plan_benefit_params)

    respond_to do |format|
      if @ifp_plan_benefit.save
        format.html { redirect_to @ifp_plan_benefit, notice: 'Ifp plan benefit was successfully created.' }
        format.json { render :show, status: :created, location: @ifp_plan_benefit }
      else
        format.html { render :new }
        format.json { render json: @ifp_plan_benefit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ifp_plan_benefits/1
  # PATCH/PUT /ifp_plan_benefits/1.json
  def update
    respond_to do |format|
      if @ifp_plan_benefit.update(ifp_plan_benefit_params)
        format.html { redirect_to @ifp_plan_benefit, notice: 'Ifp plan benefit was successfully updated.' }
        format.json { render :show, status: :ok, location: @ifp_plan_benefit }
      else
        format.html { render :edit }
        format.json { render json: @ifp_plan_benefit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ifp_plan_benefits/1
  # DELETE /ifp_plan_benefits/1.json
  def destroy
    @ifp_plan_benefit.destroy
    respond_to do |format|
      format.html { redirect_to ifp_plan_benefits_url, notice: 'Ifp plan benefit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ifp_plan_benefit
      @ifp_plan_benefit = IfpPlanBenefit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ifp_plan_benefit_params
      params[:ifp_plan_benefit]
    end
end
