require 'test_helper'

class IfpPlanBenefitsControllerTest < ActionController::TestCase
  setup do
    @ifp_plan_benefit = ifp_plan_benefits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ifp_plan_benefits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ifp_plan_benefit" do
    assert_difference('IfpPlanBenefit.count') do
      post :create, ifp_plan_benefit: {  }
    end

    assert_redirected_to ifp_plan_benefit_path(assigns(:ifp_plan_benefit))
  end

  test "should show ifp_plan_benefit" do
    get :show, id: @ifp_plan_benefit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ifp_plan_benefit
    assert_response :success
  end

  test "should update ifp_plan_benefit" do
    patch :update, id: @ifp_plan_benefit, ifp_plan_benefit: {  }
    assert_redirected_to ifp_plan_benefit_path(assigns(:ifp_plan_benefit))
  end

  test "should destroy ifp_plan_benefit" do
    assert_difference('IfpPlanBenefit.count', -1) do
      delete :destroy, id: @ifp_plan_benefit
    end

    assert_redirected_to ifp_plan_benefits_path
  end
end
