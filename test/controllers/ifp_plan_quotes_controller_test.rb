require 'test_helper'

class IfpPlanQuotesControllerTest < ActionController::TestCase
  setup do
    @ifp_plan_quote = ifp_plan_quotes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ifp_plan_quotes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ifp_plan_quote" do
    assert_difference('IfpPlanQuote.count') do
      post :create, ifp_plan_quote: {  }
    end

    assert_redirected_to ifp_plan_quote_path(assigns(:ifp_plan_quote))
  end

  test "should show ifp_plan_quote" do
    get :show, id: @ifp_plan_quote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ifp_plan_quote
    assert_response :success
  end

  test "should update ifp_plan_quote" do
    patch :update, id: @ifp_plan_quote, ifp_plan_quote: {  }
    assert_redirected_to ifp_plan_quote_path(assigns(:ifp_plan_quote))
  end

  test "should destroy ifp_plan_quote" do
    assert_difference('IfpPlanQuote.count', -1) do
      delete :destroy, id: @ifp_plan_quote
    end

    assert_redirected_to ifp_plan_quotes_path
  end
end
