require 'test_helper'

class PlansForIndividualOrFamiliesControllerTest < ActionController::TestCase
  setup do
    @plans_for_individual_or_family = plans_for_individual_or_families(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plans_for_individual_or_families)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plans_for_individual_or_family" do
    assert_difference('PlansForIndividualOrFamily.count') do
      post :create, plans_for_individual_or_family: {  }
    end

    assert_redirected_to plans_for_individual_or_family_path(assigns(:plans_for_individual_or_family))
  end

  test "should show plans_for_individual_or_family" do
    get :show, id: @plans_for_individual_or_family
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @plans_for_individual_or_family
    assert_response :success
  end

  test "should update plans_for_individual_or_family" do
    patch :update, id: @plans_for_individual_or_family, plans_for_individual_or_family: {  }
    assert_redirected_to plans_for_individual_or_family_path(assigns(:plans_for_individual_or_family))
  end

  test "should destroy plans_for_individual_or_family" do
    assert_difference('PlansForIndividualOrFamily.count', -1) do
      delete :destroy, id: @plans_for_individual_or_family
    end

    assert_redirected_to plans_for_individual_or_families_path
  end
end
