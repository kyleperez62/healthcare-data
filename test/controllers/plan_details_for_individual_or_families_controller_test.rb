require 'test_helper'

class PlanDetailsForIndividualOrFamiliesControllerTest < ActionController::TestCase
  setup do
    @plan_details_for_individual_or_family = plan_details_for_individual_or_families(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plan_details_for_individual_or_families)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plan_details_for_individual_or_family" do
    assert_difference('PlanDetailsForIndividualOrFamily.count') do
      post :create, plan_details_for_individual_or_family: {  }
    end

    assert_redirected_to plan_details_for_individual_or_family_path(assigns(:plan_details_for_individual_or_family))
  end

  test "should show plan_details_for_individual_or_family" do
    get :show, id: @plan_details_for_individual_or_family
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @plan_details_for_individual_or_family
    assert_response :success
  end

  test "should update plan_details_for_individual_or_family" do
    patch :update, id: @plan_details_for_individual_or_family, plan_details_for_individual_or_family: {  }
    assert_redirected_to plan_details_for_individual_or_family_path(assigns(:plan_details_for_individual_or_family))
  end

  test "should destroy plan_details_for_individual_or_family" do
    assert_difference('PlanDetailsForIndividualOrFamily.count', -1) do
      delete :destroy, id: @plan_details_for_individual_or_family
    end

    assert_redirected_to plan_details_for_individual_or_families_path
  end
end
